FROM node:12-alpine

RUN apk --no-cache add --virtual builds-deps build-base python
RUN apk --no-cache add git

#Setup working directory
WORKDIR '/client'

COPY package.json jsconfig.json ./
COPY ./src ./src
COPY ./public ./public

#Install dependencies
RUN yarn