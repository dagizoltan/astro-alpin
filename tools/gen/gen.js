const fs = require('fs')
const path = require('path')

const cmd = process.argv[2]
const args = process.argv.slice(3)

const generateIndexes = (path, depth = 0) => {
  const header = '/* generated */'
  const current = path.split('/').pop()
  const dir = fs.readdirSync(path, { withFileTypes: true })
  dir.forEach((x) => {
    if (x.isDirectory() && !x.name.startsWith('_')) {
      generateIndexes(path + '/' + x.name, depth + 1)
    }
  })
  let previousContent = null
  if (dir.some((x) => x.name === 'index.js')) {
    const file = fs.readFileSync(path + '/index.js', { encoding: 'utf8' })
    if (!file.startsWith(header)) {
      // skip if index does not contain generated header
      return
    }
    previousContent = file
  }
  const exports = dir.some((x) => x.name === current + '.js')
    ? [`import ${current} from './${current}'\nexport default ${current}`]
    : dir
      .map((x) => {
        if (x.isDirectory()) {
          try {
            const indexFile = fs.readFileSync(`${path}/${x.name}/index.js`, { encoding: 'utf8' })
            if (indexFile.indexOf('export default') !== -1) {
              return `export { default as ${x.name} } from './${x.name}'`
            }
          } catch (e) {}
        }
        const pts = x.name.split('.')
        if (!['js', 'ios', 'android'].includes(pts[1]) ||
            x.name === 'index.js' ||
            x.name.startsWith('_')) {
          return null
        }
        const name = pts[0]
        return `export { default as ${name} } from './${name}'`
      })
      .filter((x) => x)
  const template = header + '\n\n' + exports.join('\n') + '\n'
  const indexPath = path + '/index.js'
  if (exports.length > 0 && depth > 0) {
    if (previousContent !== template) {
      console.info('✔ ' + indexPath)
      fs.writeFileSync(indexPath, template)
    }
  }
}

const tools = {
  indexes: () => {
    generateIndexes('./src')
  },
  component: () => {
    if (args.length !== 2) {
      throw new Error('Invalid arguments.')
    }
    const module = args[0]
    const name = args[1]
    const dir = `./src/${module}/components/${name}`
    if (!fs.existsSync(`./src/${module}`)) {
      throw new Error('Module does not exists.')
    }
    if (!fs.existsSync(`./src/${module}/components`)) {
      fs.mkdirSync(`./src/${module}/components`)
    }
    if (fs.existsSync(dir)) {
      throw new Error('Component already exists.')
    }
    const templates = [
      {
        name: 'component',
        path: dir + `/${name}.js`,
        source: fs.readFileSync(
          path.join(__dirname, 'templates/component/Component.js.template'),
          { encoding: 'utf8' }
        )
      },
      {
        name: 'story',
        path: dir + `/${name}.stories.js`,
        source: fs.readFileSync(
          path.join(__dirname, 'templates/component/Component.stories.js.template'),
          { encoding: 'utf8' }
        )
      }
    ]
    const files = templates.map((x) => ({
      ...x,
      source: x.source
        .replace(/@MODULE@/g, module)
        .replace(/@NAME@/g, name)
    }))
    fs.mkdirSync(dir)
    files.forEach((x) => {
      fs.writeFileSync(x.path, x.source)
      console.info('✔ ' + x.path)
    })
    generateIndexes(dir, 2)
    generateIndexes(`./src/${module}`, 1)
  },
  screen: () => {
    if (args.length !== 2) {
      throw new Error('Invalid arguments.')
    }
    const module = args[0]
    const name = args[1]
    const dir = `./src/${module}/screens`
    const file = `${dir}/${name}Screen.js`
    if (!fs.existsSync(`./src/${module}`)) {
      throw new Error('Module does not exists.')
    }
    if (!fs.existsSync(`./src/${module}/screens`)) {
      fs.mkdirSync(`./src/${module}/screens`)
    }
    if (fs.existsSync(file)) {
      throw new Error('Screen already exists.')
    }
    const template = fs.readFileSync(
      path.join(__dirname, 'templates/screen/Screen.js.template'),
      { encoding: 'utf8' }
    )
    const source = template
      .replace(/@MODULE@/g, module)
      .replace(/@NAME@/g, name)
    fs.writeFileSync(file, source)
    console.info('✔ ' + file)
    generateIndexes(dir, 2)
  },
  container: () => {
    if (args.length !== 2) {
      throw new Error('Invalid arguments.')
    }
    const module = args[0]
    const name = args[1]
    const dir = `./src/${module}/containers`
    const file = `${dir}/${name}Container.js`
    if (!fs.existsSync(`./src/${module}`)) {
      throw new Error('Module does not exists.')
    }
    if (!fs.existsSync(`./src/${module}/containers`)) {
      fs.mkdirSync(`./src/${module}/containers`)
    }
    if (fs.existsSync(file)) {
      throw new Error('Container already exists.')
    }
    const template = fs.readFileSync(
      path.join(__dirname, 'templates/container/Container.js.template'),
      { encoding: 'utf8' }
    )
    const source = template
      .replace(/@MODULE@/g, module)
      .replace(/@NAME@/g, name)
    fs.writeFileSync(file, source)
    console.info('✔ ' + file)
    generateIndexes(dir, 2)
  },
  module: () => {
    if (args.length !== 1) {
      throw new Error('Invalid arguments.')
    }
    const module = args[0]
    const dir = `./src/${module}`
    if (fs.existsSync(dir)) {
      throw new Error('Module already exists.')
    }
    fs.mkdirSync(dir)
    fs.mkdirSync(dir + '/services')
    fs.mkdirSync(dir + '/components')
    fs.mkdirSync(dir + '/containers')
    fs.mkdirSync(dir + '/screens')
    fs.mkdirSync(dir + '/types')
    fs.mkdirSync(dir + '/constants')
    fs.mkdirSync(dir + '/context')
  }
}

if (tools[cmd]) {
  tools[cmd]()
} else {
  throw new Error('Invalid command.')
}
