import { Box, Button, CardMedia, Container, Grid, Link, Typography } from '@material-ui/core'
import React from 'react'
import BussinessCardIcon from './BussinessCardIcon.svg'
import useStyles from './useStyles'

function LargeToolbar(props) {
  const classes = useStyles()

  return (
    <Box>
      <Box py={2} bgcolor='#262626'>
        <Container>
          <Grid container justify='flex-end'>
            <Box px={1}>
              <Typography > <Link href='mailto:info@astro-alpin.hu' style={{ color: '#ffffff' }} >info@astro-alpin.hu</Link> | </Typography>
            </Box>
            <Box pl={1}>
              <Link href='tel:+36308781085' style={{ color: '#ffffff' }}><Typography>+36 30 878 1085</Typography></Link>
            </Box>
          </Grid>
        </Container>
      </Box>
      <Box>
        <Container>
          <Grid container direction='row' justify='space-between' alignItems='center'>
            <Box py={2}>
              <Grid container alignItems='center'>
                <Box mr={2} py={2}><CardMedia component='img' src={BussinessCardIcon} style={{ height: 45, width: 45 }} /></Box>
                <Grid item>
                  <Grid container direction='column'>
                    <Grid item>
                      <Grid container>
                        <Box mr={1}><Typography variant='h2' style={{ color: '#ffffff' }}>Astro </Typography></Box>
                        <Typography variant='h2' color='primary' style={{ fontWeight: 'bold' }}>Alpin</Typography>
                      </Grid>

                    </Grid>
                    <Grid item>
                      <Typography variant='caption'>Budapest</Typography>
                    </Grid>
                  </Grid>

                </Grid>

              </Grid>

            </Box>

            <Box py={2}>
              <Link href='#szolgaltatasok' underline='none'><Button>Szolgáltatások</Button></Link>
              <Link href='#referenciak' underline='none'><Button>Referenciák</Button></Link>
              <Link href='#about' underline='none'><Button>A csapat</Button></Link>
              <Link href='#elerhetosegek' underline='none'><Button>Elérhetöségeink</Button></Link>
              <Link href='mailto:info@astro-alpin.hu?subject=Àrajánlat Kérés' underline='none'><Button variant='contained' color='primary'>Àrajánlat kéres</Button></Link>
            </Box>
          </Grid>
        </Container>
      </Box>
    </Box>
  )
}

export default LargeToolbar
