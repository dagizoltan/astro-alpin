import React from 'react'

import {
  Box,
  Button,
  Link,
  Toolbar as T
} from '@material-ui/core'

function Toolbar (props) {
  const tools = [
    { href: '#szolgaltatasok', label: 'Szolgáltatások' },
    { href: '#referenciak', label: 'Referenciák' },
    { href: '#rolunk', label: 'A csapat' },
    { href: '#elerhetosegek', label: 'Elérhetöségeink' }
  ]

  return (
    <T disableGutters>
      <Box>
        {tools.map(({ href, label }) => <Link key={href} href={href}><Button>{label}</Button></Link>)}
      </Box>
    </T>
  )
}

export default Toolbar
