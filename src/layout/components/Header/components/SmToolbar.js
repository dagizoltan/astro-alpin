import {
  AppBar,

  Box,
  Button, CardMedia,
  Container,
  Grid,
  Link, Typography
} from '@material-ui/core'
import React from 'react'
import BussinessCardIcon from './BussinessCardIcon.svg'
import Toolbar from './Toolbar'

function SmallToolbar () {
  return (
    <Box p={2}>
      <Container>
        <Grid container direction='row' justify='space-between' alignItems='center'>
          <Box mb={2}>
            <Grid container alignItems='center'>
              <Box mr={2}><CardMedia component='img' src={BussinessCardIcon} style={{ height: 40, width: 40 }} /></Box>
              <Box mr={1}><Typography variant='h1' style={{ color: '#ffffff' }}>Astro </Typography></Box>
              <Typography variant='h1' color='primary' style={{ fontWeight: 'bold' }}>Alpin </Typography>
            </Grid>
          </Box>
          <Box>
            <Grid container direction='column' alignItems='flex-end' justify='center'>
              <Link href='tel:+36708781085' style={{ color: '#ffffff' }}><Typography variant='caption'>+36 30 878 1085</Typography></Link>
              <Link href='mailto:info@astro-alpin.hu' style={{ color: '#ffffff' }}><Typography variant='caption'>info@astro-alpin.hu</Typography></Link>
            </Grid>
          </Box>
          <Box>
            <Box py={2}>
              <Link href='mailto:info@astro-alpin.hu?subject=Àrajánlat Kérés'><Button variant='contained' color='primary' fullWidth>Àrajánlat kéres</Button></Link>
            </Box>
            <AppBar position='sticky' style={{ background: 'transparent', boxShadow: 'none' }}>
              <Toolbar />
            </AppBar>
          </Box>
        </Grid>
      </Container>
    </Box>
  )
}

export default SmallToolbar
