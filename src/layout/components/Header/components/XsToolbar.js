import React, { useState } from 'react'
import IconButton from '@material-ui/core/IconButton'

import MenuIcon from '@material-ui/icons/Menu'

import { AppBar, Toolbar, Card, CardActionArea, CardContent, CardMedia, Box, Button, Container, Divider, Grid, Typography, Link, List, ListItem, ListSubheader } from '@material-ui/core'
import BussinessCardIcon from './BussinessCardIcon.svg'

import Breadcrumb from 'layout/components/Breadcrumb'
import useStyles from './useStyles'

function SmallToolbar(props) {
  const classes = useStyles()
  const [isOpen, setIsOpen] = useState(false);

  const handleMenuClick = () => {
    setIsOpen(!isOpen)
  }
  return (
    <Box>
      <Container >
        <Grid container direction='row' justify='space-between' alignItems='center'>
          <Box py={2}>
            <Grid container alignItems='center'>
              <Box mr={2}><CardMedia component='img' src={BussinessCardIcon} style={{ height: 32, width: 32 }} /></Box>
              <Box mr={1}><Typography variant="h1"  style={{ color: '#ffffff'}}>Astro </Typography></Box>
                <Typography variant="h1" color='primary' style={{ fontWeight: 'bold' }}>Alpin </Typography>
            </Grid>
          </Box>

          <Box><IconButton onClick={handleMenuClick}><MenuIcon /></IconButton></Box>
            
        </Grid>
        {isOpen
              ? (
                <Box mx={-1} pb={2}>
                  <Grid container direction='column' alignItems='flex-start'>
                  <Link href='mailto:info@astro-alpin.hu?subject=Àrajánlat Kérés'><Button variant='contained' color='primary' onClick={() => setIsOpen(!isOpen)}>Àrajánlat kéres</Button></Link>
                  <Link href='#szolgaltatasok'><Button onClick={() => setIsOpen(!isOpen)}>Szolgáltatások</Button></Link>
                  <Link href='#referenciak'><Button onClick={() => setIsOpen(!isOpen)}>Referenciák</Button></Link>
                  <Link href='#about'><Button onClick={() => setIsOpen(!isOpen)}>A csapat</Button></Link>
                  <Link href='#elerhetosegek' onClick={() => setIsOpen(!isOpen)}><Button>Elérhetöségeink</Button></Link>
                  </Grid>
                </Box>)
              : null
            }
      </Container>
    </Box>
  )
}

export default SmallToolbar
