import { Box, Button, CardMedia, Container, Grid, Link, Typography } from '@material-ui/core'
import React from 'react'
import BussinessCardIcon from './BussinessCardIcon.svg'
import useStyles from './useStyles'

function SmToolbar (props) {
  const classes = useStyles()

  return (
    <Box>
      <Box p={2} bgcolor='#262626'>
        <Container>
          <Grid container justify='flex-end'>
            <Box px={1}>
              <Link href='mailto:info@astro-alpin.hu' style={{ color: '#ffffff' }}><Typography variant='caption'>info@astro-alpin.hu |</Typography></Link>
            </Box>
            <Box pl={1}>
              <Link href='tel:+36708781085' style={{ color: '#ffffff' }}><Typography variant='caption'>+36 30 878 1085</Typography></Link>
            </Box>
          </Grid>
        </Container>
      </Box>
      <Box p={2}>
        <Container>
          <Grid container direction='row' justify='space-between' alignItems='center'>
            <Box>
              <Grid container alignItems='center'>
                <Box mr={2} py={2}><CardMedia component='img' src={BussinessCardIcon} style={{ height: 32, width: 32 }} /></Box>
                <Box mr={1}><Typography variant='h1' style={{ color: '#ffffff' }}>Astro </Typography></Box>
                <Typography variant='h1' color='primary' style={{ fontWeight: 'bold' }}>Alpin</Typography>
              </Grid>

            </Box>
            <Box>
              <Grid
container
                direction='column'
                justify='center'
                alignItems='flex-end'
              >
                <Box>
                  <Link href='mailto:info@astro-alpin.hu?subject=Àrajánlat kérés'><Button variant='contained' color='primary'>Àrajánlat kéres</Button></Link>
                </Box>

              </Grid>
            </Box>

          </Grid>
          <Grid container justify='flex-end'>
            <Box py={1}>
              <Link href='#szolgaltatasok'><Button>Szolgáltatások</Button></Link>
              <Link href='#referenciak'><Button>Referenciák</Button></Link>
              <Link href='#about'><Button>A csapat</Button></Link>
              <Link href='#elerhetosegek'><Button>Elérhetöségeink</Button></Link>
            </Box>
          </Grid>

        </Container>
      </Box>
    </Box>
  )
}

export default SmToolbar
