import React from 'react'

import {
  AppBar,
  useMediaQuery
} from '@material-ui/core'

import {
  SmToolbar, LargeToolbar, MdToolbar, XsToolbar
} from './components'

function Header () {
  const toolbars = {
    lg: LargeToolbar,
    md: MdToolbar,
    sm: SmToolbar,
    xs: XsToolbar
  }

  let type = 'xs'

  if (useMediaQuery(theme => theme.breakpoints.up('sm'))) type = 'sm'
  if (useMediaQuery(theme => theme.breakpoints.up('md'))) type = 'md'
  if (useMediaQuery(theme => theme.breakpoints.up('lg'))) type = 'lg'
  console.log(type)
  const ToolbarComponent = toolbars[type]

  return (
    <>
      <AppBar color='dark'>
        <ToolbarComponent />
      </AppBar>
    </>
  )
}

export default Header
