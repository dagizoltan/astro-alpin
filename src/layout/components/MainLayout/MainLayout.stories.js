import React from 'react'
import MainLayout from './MainLayout'

export default { title: 'mainlayout.MainLayout' }

export const normal = () =>
  <MainLayout>
    content
  </MainLayout>
