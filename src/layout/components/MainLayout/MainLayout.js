import React from 'react'
import FetchBar from '../FetchBar'
import Header from '../Header'
import DashboardScreen from 'modules/dashboard/DashboardScreen'
import { Box, useMediaQuery } from '@material-ui/core'

function MainLayout (props) {
  const { children } = props

  let topMargin

  if (useMediaQuery(theme => theme.breakpoints.up('sm'))) topMargin = 4
  else topMargin = 2

  return (
    <>
      <FetchBar />
        <Box mt={topMargin}>
          <Header />
          <DashboardScreen />
        </Box>
    </>
  )
}

export default MainLayout
