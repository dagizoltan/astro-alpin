import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  box: {
    background: theme.palette.dark
  },
  caption: {
    color: theme.palette.light
  }
})
)

export default useStyles
