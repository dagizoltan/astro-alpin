import React from 'react'
import { Box, Typography, Divider } from '@material-ui/core'
import useStyles from './useStyles'
function Footer (props) {
  const { box,caption } = useStyles()
  return (
    <>
      <Divider />
      <Box p={1} className={box}>
        <Typography variant='caption' className={caption}>© 2020 MPF Venture Capital and Technology</Typography>
      </Box>
    </>
  )
}

export default Footer
