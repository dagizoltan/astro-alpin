import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  '@keyframes breadcrumbSlide': {
    '0%': { marginLeft: 100, opacity: 0 },
    '100%': { marginLeft: 0, opacity: 1 }
  },
  container: {
    display: 'flex',
    background: '#fff',
    borderBottom: '1px solid #ccc'
  },
  menuButton: {
    cursor: 'pointer',
    color: '#444',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 5,
    marginRight: 10,
    paddingBottom: 3
  },
  items: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row'
  },
  item: {
    padding: 10,
    paddingLeft: 20,
    position: 'relative',
    animation: '$breadcrumbSlide 0.5s ease-in',
    '&:first-child': {
      paddingLeft: 0
    }
  }
}))

export default useStyles
