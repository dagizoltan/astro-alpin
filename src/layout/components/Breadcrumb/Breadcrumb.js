import React, { useMemo } from 'react'
import { useNavigation } from 'navigation/hooks'
import { useLocale } from 'locale'
import { Box, useMediaQuery } from '@material-ui/core'
import { useTheme } from '@material-ui/styles'
import useStyles from './useStyles'

function Breadcrumb (props) {
  const { module, screens, pop } = useNavigation()
  const { __ } = useLocale()
  const classes = useStyles()
  const theme = useTheme()
  const showFullBreadcrumb = useMediaQuery(theme.breakpoints.up('md'))

  const items = useMemo(() =>
    showFullBreadcrumb
      ? !module.routes ? [module] : [module, ...screens]
      : screens.slice(-1)
  , [showFullBreadcrumb, module, screens])

  return (
    <Box className={classes.items}>
      {items.map((x, i) =>
        <Box key={i} className={classes.item}>
          {i === 0 && showFullBreadcrumb
            ? <Box style={{ fontWeight: '500' }}>{__(x.title)}</Box>
            : i === items.length - 1
              ? <Box>{__(x.title)}</Box>
              : <a style={{ color: '#aaa' }} href={x.location} onClick={(e) => { e.preventDefault(); pop(items.length - i - 1) }}>{__(x.title)}</a>}
        </Box>
      )}
    </Box>
  )
}

export default Breadcrumb
