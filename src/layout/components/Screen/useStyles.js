import { makeStyles } from '@material-ui/styles'
import { keyframes } from 'emotion'

const slide = keyframes`
  0% { left: 300px; opacity: 0; }
  100% { left: 0px; opacity: 1 }
`
const useStyles = makeStyles(theme => ({
  sx: {
    animation: `${slide} 0.5s ease-in`,
    opacity: ({ active }) => active ? 1 : 0.3,
    position: 'absolute',
    top: '0px',
    left: '0px',
    bottom: '0px',
    right: '0px',
    // background: theme.palette.light,
    padding: '10px',
    transition: 'opacity 0.5s',
    overflowY: 'scroll',
    '&::-webkit-scrollbar': {
      width: 0, /* Remove scrollbar space */
      background: 'transparent'
    }
  }
}))

export default useStyles
