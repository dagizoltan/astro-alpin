import React, { useMemo } from 'react'
import { Box } from '@material-ui/core'
import { ScreenContext } from 'core/screen'

import useStyles from './useStyles'

function Screen (props) {
  const { children, active } = props

  const contextValue = useMemo(() => {
    return {
      isActive: active
    }
  }, [active])

  const classes = useStyles({ active })
  return (
    <ScreenContext.Provider value={contextValue}>
      <Box className={classes.sx}>
        {children}
      </Box>
    </ScreenContext.Provider>
  )
}

export default Screen
