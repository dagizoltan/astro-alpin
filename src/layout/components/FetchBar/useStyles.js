import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  container: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 100
  },
  progressRoot: {
    height: 2,
    background: '#aaa',
    opacity: ({ isFetching }) => isFetching ? 1 : 0,
    transition: 'opacity 0.5s ease'
  },
  progressBar: {
    background: '#222'
  }
}))

export default useStyles
