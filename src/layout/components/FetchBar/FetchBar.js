import React, { useState, useEffect } from 'react'
import { Box, LinearProgress } from '@material-ui/core'
import useStyles from './useStyles'
import { requestCounter } from 'core/http'

function FetchBar (props) {
  const [isFetching, setIsFetching] = useState(false)
  const classes = useStyles({ isFetching })

  useEffect(() => {
    const cb = (requestCount) => {
      setIsFetching(requestCount > 0)
    }
    const unsubscribe = requestCounter.subscribe(cb)
    return () => {
      unsubscribe()
    }
  }, [setIsFetching])

  return (
    <Box className={classes.container}>
      <LinearProgress classes={{ root: classes.progressRoot, bar: classes.progressBar }} />
    </Box>
  )
}

export default FetchBar
