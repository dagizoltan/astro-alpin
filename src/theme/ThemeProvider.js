import React from 'react'
import usePalette from './palette/usePalette'
import { createMuiTheme, responsiveFontSizes, ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'

function ThemeProvider (props) {
  const { children } = props

  const { palette, type } = usePalette()

  let theme = {
    palette: { ...palette, type },
    shape: {
      borderRadius: 2
    },
    spacing: [0, 5, 10, 20, 100],

    breakpoints: {
      values: {
        xs: 0,
        sm: 520,
        md: 550,
        lg: 1000,
        xl: 1920
      },
    },
    typography: {
      h1: {
        color: palette.primary.dark,
        fontSize: '2rem',
        fontWeight: '400'
      },
      h2: {
        color: palette.primary.main,
        fontSize: '1.545rem'
      },
      h3: {
        color: palette.primary.main,
        fontSize: '0.955rem'
      },
      h4: {
        color: palette.primary.dark,
        fontSize: '0.955rem'
      },
      h5: {
        color: palette.primary.dark,
        fontSize: '1.545rem'
      },
      body2: {
        fontSize: '0.955rem'
      }
    }
  }

  theme = createMuiTheme(theme)
  theme = responsiveFontSizes(theme)

  return (
    <MuiThemeProvider theme={theme}>
      {children}
    </MuiThemeProvider>
  )
}

export default ThemeProvider
