const palette = {
  primary: {
    main: '#a5be00',
    dark: '#303030'
  },
  secondary: {
    main: '#48a999'
  },
  error: {
    main: '#cc0066'
  },
  warning: {
    main: '#fca311'
  },
  info: {
    main: '#003366'
  },
  success: {
    main: '#8bbf18'
  },
  light: {
    main: '#ffffff'
  },
  contrastThreshold: 3,
  tonalOffset: 0.2
}

export default palette
