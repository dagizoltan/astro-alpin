import React from 'react'
import { Box, CardMedia, Container, Grid, Typography } from '@material-ui/core'

import GroupArtwork from './images/GroupArtwork.svg'
import Team from './images/Team.jpg'
import Team1 from './images/Team1.jpg'
import Team2 from './images/Team2.jpg'
import Team3 from './images/Team3.jpg'
import Team4 from './images/Team4.jpg'

import useStyles from './useStyles'

function About () {
  const classes = useStyles()

  const teamGallery = [
    Team1,
    Team2,
    Team3,
    Team4
  ]
  return (
    <Box style={{ backgroundImage: `url(${Team})`, backgroundSize: 'cover', backgroundRepeat: 'no-repeat', backgroundPosition: 'center' }}>

      <Box className={classes.overlay} py={4} px={2} id='about' style={{ scrollMarginTop: '100px' }}>

        <Container>
          <Box>
            <Typography variant='h2' paragraph>A csapat</Typography>
            <Typography paragraph>
              Jelenleg fő profilunk a külső-belső ablaktisztítás.
            </Typography>
            <Typography paragraph>
              Alpinistáink szakszerű kivitelezését garantálja az ipari alpintechnikában átlagosan eltöltött több mint 8 év.
            </Typography>
            <Typography paragraph>
              Csapatunk fix létszáma 7 fő, ami a nagyobb kaliberű munkák elvégzése érdekében bármikor bővíthető 15 főre, így maximalizálva a megrendelőnek a teljesség igényét.
            </Typography>

            <Typography paragraph>
              Csapatunk tagjai a magasban végzett szakszerű munkafolyamatok mellett egyéb szakmunkákban is  jártasak, így tapasztalataink sokaságában fellelhető más magasban végzett munka is, mint például emelőgéppel való munkavégzés, a villanyszerelő, ács, kőműves, nyílászáró- árnyékolástechnikai munkák kivitelezése is.
            </Typography>
            <Typography paragraph>
              Ezen felül aplinistáink ugyanolyan precizitással vállalják egyaránt társasházak homlokzatának veszélytelenítését és panelhézagolást is.
            </Typography>
            <Typography paragraph>
              Egyéb látványos munkákban való részvételünk közé tartozik a Bécsben szánkóból, Kőbányán pedig farönkökből épített karácsonyfák elkészítése.
            </Typography>
            <Typography paragraph>
              Munkatársaink rendelkeznek a megfelelő képesítésekkel és az ipari alpintechnikai felszerelések bevizsgálására alkalmas személy is megtalálható nálunk.
            </Typography>
          </Box>
        </Container>

      </Box>

    </Box>
  )
}

export default About
