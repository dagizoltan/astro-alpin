import React from 'react'
import { Box, Grid, } from '@material-ui/core'
import Corvin from './images/Corvin.jpg'
import * as Images from './images/gallery'

import useStyles from './useStyles'

function References () {
  const classes = useStyles()

  function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}
  let images = Object.values(Images)
  shuffle(images)
  const square = 100/12
  return (
    <Box>
      <Grid
        container
        direction='row'
        justify='flex-start'
        alignItems='stretch'
      >
        {images.map(img => (
          <Grid item xs={1} sm={1} md={1} lg={1}>
            <Box style={{ height: `${square}vw`, backgroundImage: `url(${img})`, backgroundSize: 'cover', backgroundRepeat: 'no-repeat', backgroundPosition: 'center' }}>
            </Box>
          </Grid>)
        )}
      </Grid>
    </Box>
  )
}

export default References
