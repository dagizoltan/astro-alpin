import React from 'react'
import { Box, Button, Container, CardMedia, Grid, Typography, Link, List, ListItem, ListSubheader } from '@material-ui/core'

import ServicesBackground from './images/ServicesBackground.jpg'
import useStyles from './useStyles'
import Artwork from './images/GroupArtwork.svg'

function Services() {
  const classes = useStyles()
  const services = {
    main: [
      'Eresz takarítás',
      'Ázás elhárítás',
      'Külsö, belsö ablaktisztítás',
      'Zsaluzia takarítás magasnyomású mosóval',
      'Homlokzat tisztítás magasnyomású mosóval',
      'Szerkezet takarítás',
      'Ipari alpintechnikai felszerelések bevizsgálása'
    ],
    other: [
      'Villanyszerelés',
      'Àcsolás',
      'Kömǘves munkálatok',
      'Térkötisztítás magasnyomású mosóval',
      'Nyílászáró- és árnyékolástechnika'
    ]
  }

  return (
    <Box>
      <Box style={{ backgroundImage: `url(${ServicesBackground})`, backgroundSize: 'cover', backgroundRepeat: 'no-repeat', backgroundPosition: 'center' }}>
        <Box className={classes.overlay} py={4} px={2} id='szolgaltatasok' style={{ scrollMarginTop: '100px' }} >
          <Container>
            <Typography variant='h2' paragraph>Szolgáltatások</Typography>
            <Grid container justify='space-between'>
              <Grid md={6}>
                <List>
                  <ListSubheader disableGutters disableSticky>Alpintechnika</ListSubheader>
                  {
                    services.main.map(s => (
                      <ListItem disableGutters key={s}>
                        {s}
                      </ListItem>)
                    )
                  }
                </List>
              </Grid>
              <Grid md={6}>
                <List>
                  <ListSubheader disableGutters disableSticky>Egyéb</ListSubheader>
                  {services.other.map(s => <ListItem disableGutters>{s}</ListItem>)}
                </List>
              </Grid>

            </Grid>
            <Box py={2}>
              <Box mb={2}>
                <Typography color='primary'>Ingyenes munkafelmérés és árajánlat kérés</Typography>

              </Box>
              <Link href='mailto:info@astro-alpin.hu?subject=Àrajánlat kérés'><Button variant='contained' color='primary'>Àrajánlat kéres</Button></Link>
            </Box>
          </Container>
        </Box>
      </Box>
    </Box>
  )
}

export default Services
