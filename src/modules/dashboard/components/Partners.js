import React from 'react'
import { Card, CardActionArea, CardContent, Box, Container, Grid, Typography } from '@material-ui/core'

function Partners () {
  const partners = [
    'CSS-FM Kft.',
    'Clean House Kft.',
    'Fénysziget Kft.',
    'Bianka Property Kft.'
    // "Vilmos Elemér Consulting Kft."
  ]

  return (
    <Box py={4} px={2} id='partnerek' style={{ scrollMarginTop: '100px' }}>
      <Container>
        <Box>
          <Typography variant='h2' paragraph>Partnerek</Typography>
          <Grid
            container
            direction='row'
            justify='flex-start'
            alignItems='stretch'
          >
            {partners.map(r => (
              <Grid item xs={12} sm={6} md={6} xl={3}>
                <Card>
                  <CardActionArea>
                    <CardContent>
                      <Typography variant='h3>'>{r}</Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )
            )}
          </Grid>
        </Box>

      </Container>
    </Box>
  )
}

export default Partners
