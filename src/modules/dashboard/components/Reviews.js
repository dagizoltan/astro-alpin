import React from 'react'
import { Card, CardActionArea, CardMedia, CardContent, Box, Container, Grid, Typography } from '@material-ui/core'
import StarIcon from '@material-ui/icons/Star';
import GroupArtwork from './images/GroupArtwork.svg'

import useStyles from './useStyles'

function Reviews() {
    const classes = useStyles()
    const reviews = [
        {
            name: 'Anita Károlyi',
            rating: 5,
            comment: 'Gyorsan és profin dolgoznak, korrekt árazás mellett',
            services: 'Eresztakarítás, Üveg- és tükörtisztítás, Gépi/magas nyomású mosás, Tető és tetőablak tisztítása, Ablaktisztítás',
            src: 'Google'
        },
        {
            name: 'Stewe',
            rating: 5,
            comment: 'Nagyon kedves tisztelet tudó csapat pontos precíz profi munka',
            services: 'Eresztakarítás, Üveg- és tükörtisztítás, Gépi/magas nyomású mosás, Tető és tetőablak tisztítása, Ablaktisztítás',
            src: 'Google'
        },
        {
            name: 'István Sajbán',
            rating: 5,
            comment: 'Precízen - határidőben teljesítették a még rendelés!',
            services: 'Ablaktisztítás',
            src: 'Google'
        },
        {
            name: 'Péter Nagy',
            rating: 5,
            comment: '',
            services: 'Tető és tetőablak tisztítása, Üveg- és tükörtisztítás, Ablaktisztítás, Gépi/magas nyomású mosás, Eresztakarítás',
            src: 'Google'
        },
    ]
    return (
        <Box py={4} px={2} id='about' style={{ scrollMarginTop: '100px' }}>

            <Container>
                <Box>
                    <Typography variant='h2' paragraph>Értékelések</Typography>
                    <Grid
                        container
                        direction='row'
                        justify='flex-start'
                        alignItems='stretch'
                    >
                        {reviews.map(r => (
                            <Grid item xs={12} sm={6} md={6} xl={3}>
                                <Box py={2}>
                                    <Box py={2}>
                                            <Box mb={2}>

                                            </Box>

                                                {(() => {
                                                    const rows = [];
                                                    for (let i = 0; i < r.rating; i++) {
                                                        rows.push(<StarIcon />);
                                                    }
                                                    return rows;
                                                })()}


                                    </Box>
                                    <Box py={2}>
                                        <Typography variant='paragraph'>{r.comment}</Typography>
                                    </Box>
                                    <Box py={2}>
                                        <Typography variant='h3'>Igénybe vett szolgáltatások</Typography>
                                        <Typography variant='caption'>{r.services}</Typography>
                                    </Box>
                                </Box>


                            </Grid>
                        )
                        )}
                    </Grid>
                </Box>

            </Container>
        </Box>
    )
}

export default Reviews
