import React from 'react'
import { Card, CardActionArea, CardMedia, Box, Container, Grid, Typography } from '@material-ui/core'
import Corvin from './images/Corvin.jpg'
import ParizsiNagyaAruhaz from './images/ParizsiNagyAruhaz.jpg'
import InfoPark from './images/InfoPark.jpg'
import RTL from './images/RTL.jpg'
import Westend from './images/Westend.jpg'
import MOMPark from './images/MomPark.jpg'
import Stadion from './images/Stadion.jpg'
import CEU from './images/CEU.jpg'
import C5 from './images/C5.jpg'
import Bar360 from './images/360Bar.jpg'

import useStyles from './useStyles'

function References () {
  const classes = useStyles()
  const references = [
    {
      building: 'MOM Park',
      img: MOMPark,
      desc: 'Ablakmosás'
    },
    {
      building: 'RTL Székház',
      img: RTL,
      desc: 'Ablakmosás'
    },
    {
      building: 'Párizsi Nagy Áruház',
      img: ParizsiNagyaAruhaz,
      desc: 'Ablakmosás'
    },
    {
      building: '360 bar',
      img: Bar360,
      desc: 'Szerkezettakaritás'
    },
    {
      building: 'InfoPark',
      img: InfoPark,
      desc: 'Zsaluzia (árnyékoló) portalanítás'
    },
    {
      building: 'West End City Center',
      img: Westend,
      desc: 'Ablakmosás és szerkezettakarítás'
    },
    {
      building: 'Corvin irodaház',
      img: Corvin,
      desc: 'Ablakmosás'
    },
    {
      building: 'CEU',
      img: CEU,
      desc: 'Ablakmosás'
    },
    {
      building: 'C5',
      img: C5,
      desc: 'Ablakmosás'
    },
    {
      building: 'Stadion (HÉG Kft.)',
      img: Stadion,
      desc: 'Sprinkler-rendszer szerelés'
    },

  ]

  return (
    <Box>
      <Box py={4} px={2} id='referenciak' style={{ scrollMarginTop: '100px' }}>
        <Container>
          <Typography variant='h2' paragraph>Referenciák</Typography>
          <Typography paragraph>A szakmában eltöltött több mint 8 év alatt felsorolható különböző profilú munkák összegyűjtése nem könnyű feladat, így alább olvasható néhány fontosabb a teljesség igenye nélkül</Typography>
        </Container>
      </Box>

      <Grid
        container
        direction='row'
        justify='flex-start'
        alignItems='stretch'
      >
        {references.map(r => (
          <Grid item xs={12} sm={6} md={4} lg={4}>
            <Card>
              <CardActionArea>
                <CardMedia image={r.img} style={{ width: '100%', height: 300, position: 'relative' }} />
                <Box style={{ zIndex: 1000, position: 'absolute', bottom: '0px', left: '0px', width: '100%' }}>
                  <Box py={3} px={2} className={classes.overlay}>
                    <Box mb={1}>
                      <Typography variant='h3'>{r.building}</Typography>
                    </Box>
                    <Typography variant='h3>'>{r.desc}</Typography>
                  </Box>
                </Box>
              </CardActionArea>
            </Card>
          </Grid>)
        )}
      </Grid>
    </Box>
  )
}

export default References
