import { makeStyles, fade } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({
  overlay: {
    backgroundColor: fade(theme.palette.primary.dark, 0.8)
  }

}))

export default useStyles
