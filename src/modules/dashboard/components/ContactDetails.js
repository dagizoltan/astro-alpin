import React from 'react'
import { CardMedia, Box, Container, Grid, Typography, Link, ListItem, useMediaQuery } from '@material-ui/core'
import LogoTrimmed from './images/LogoTrimmed.svg'


function ContactDetails() {
  const isXs = useMediaQuery(theme => theme.breakpoints.down('sm'))

  return (
    <Box py={4} px={2} id='elerhetosegek' style={{ scrollMarginTop: '100px' }}>
      <Container>
        <Grid container justify='space-between' alignItems='center'>
          <Grid item>
            <Box mb={2} mr={2}>
              <Box>
                <Box mb={2}>
                  <Typography variant='h2' paragraph>Elérhetöségeink</Typography>
                  <Typography variant='h3' style={{ color: '#FFFFFF' }}>Leányvári Richárd</Typography>
                  <Typography variant='caption'>Koordinátor és Alpinista</Typography>
                </Box>
                <Link href='mailto:info@astro-alpin.hu'>
                  <Typography>info@astro-alpin.hu</Typography>
                </Link>
                <ListItem disableGutters dense>
                  <Link href='tel:+36308781085'>
                    <Typography>+36 30 878 1085</Typography>
                  </Link>
                </ListItem>
              </Box>
            </Box>
          </Grid>
          <Grid item>
            <Box>
              <Grid container direction='row' justify='center' alignItems='flex-start'>
                <Grid item>
                  <Grid container direction='row' justify='center' alignItems='center'>
                    {
                      !isXs
                        ? (
                          <Grid item>
                          <Grid container direction='column' alignItems='flex-end'>
                            <Grid item>
                              <Grid container>
                                <Box mr={1}><Typography variant='h1' style={{ color: '#ffffff' }}>Astro </Typography></Box>
                                <Box><Typography variant='h1' color='primary' style={{ fontWeight: 'bold' }}>Alpin</Typography></Box>
                              </Grid>
                            </Grid>
                            <Grid item>
                              <Typography variant='caption'>Budapest</Typography>
                            </Grid>
                          </Grid>
                          </Grid>
                        )
                        : null
                    }

                    <CardMedia component='img' src={LogoTrimmed} style={{ height: 125, width: 75 }} />
                  </Grid>
                </Grid>
              </Grid>
              <Grid container />
            </Box>
          </Grid>

        </Grid>
      </Container>
    </Box>
  )
}

export default ContactDetails
