import React from 'react'
import { Box, Container, Divider } from '@material-ui/core'

import {
  Services,
  References,
  Partners,
  About,
  Reviews,
  Gallery,
  ContactDetails
} from './components'

function DashboardScreen (props) {
  return (
    <Box>
      <Services />
      <Divider />
      <References />
      <Divider />
      <Partners />
      <Divider />
      <About />
      <Divider />
      <Reviews />
      <Divider />
      <Gallery />
      <Divider />
      <ContactDetails />
    </Box>
  )
}

export default DashboardScreen
