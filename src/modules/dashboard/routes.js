import { faBorderAll } from '@fortawesome/free-solid-svg-icons'
import DashboardScreen from './DashboardScreen'

export default {
  menuItem: true,
  title: { en: 'Dashboard', hu: 'Főoldal' },
  icon: faBorderAll,
  path: { en: '/', hu: '/' },
  component: DashboardScreen
}
