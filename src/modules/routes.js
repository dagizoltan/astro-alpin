// import { routes as articlesRoutes } from 'modules/articles'
import { routes as dashboardRoutes } from './dashboard'

const routes = [
  dashboardRoutes
]

export default routes
