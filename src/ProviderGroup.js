import React from 'react'

import ThemeProvider from './theme/ThemeProvider'
import PaletteProvider from './theme/palette/PaletteProvider'

import { CssBaseline } from '@material-ui/core'
import { HttpProvider } from 'core/http'
import LocaleProvider from 'locale/LocaleProvider'
import { NavigationProvider } from './navigation/context/NavigationContext'
import { WebSocketProvider } from 'core/websocket'
import { AuthProvider } from './auth/context/AuthContext'
import { ListenerProvider } from 'core/listener/ListenerContext'
import { NotificationProvider } from 'core/notification'

export function ProviderGroup (props) {
  const { children } = props
  return (
    <PaletteProvider>
      <ThemeProvider>
        <CssBaseline>
          <LocaleProvider>
            <NavigationProvider>
              <AuthProvider>
                {/*<WebSocketProvider>*/}
                  <ListenerProvider>
                    <HttpProvider>
                      <NotificationProvider>
                        {children}
                      </NotificationProvider>
                    </HttpProvider>
                  </ListenerProvider>
                {/*</WebSocketProvider>*/}
              </AuthProvider>
            </NavigationProvider>
          </LocaleProvider>
        </CssBaseline>
      </ThemeProvider>
    </PaletteProvider>
  )
}

export default ProviderGroup
