import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  screen: {
    height: '100vh',
    background: theme.palette.primary.dark,
  },
  gridItem: {
    marginBottom: theme.spacing(2)
  },
  card: {
    background: theme.palette.primary.main,
  },
  logo: {
    background: theme.palette.primary.light,
    borderRadius: '50%',
    paddingTop: '22px',
    height: '70px',
    width: '70px',
    verticalAlign: 'middle',
    textAlign: 'center'
  }
}))

export default useStyles
