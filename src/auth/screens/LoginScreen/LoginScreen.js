import React, { useState } from 'react'
import {
  withStyles
} from '@material-ui/core/styles'
import { Box, Card, Fade, TextField, Button, Backdrop, Grid, CircularProgress, Typography } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { useAuth } from '../../hooks'
import { useLocale } from 'locale'
import useStyles from './useStyles'

const TextFieldLight = withStyles({
  root: {
    '& label': {
      color: 'white'
    },
    '& .MuiInput-underline': {
      borderBottomColor: 'white'
    },
    '& label.Mui-focused': {
      color: 'white'
    },
    '& .MuiInput-underline:after': {
      borderBottomColor: 'white'
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'white'
      },
      '&:hover fieldset': {
        borderColor: 'white'
      },
      '&.Mui-focused fieldset': {
        borderColor: 'white'
      }
    }
  }
})(TextField)

function LoginScreen(props) {
  const { _ } = useLocale()
  const { login, loading, error } = useAuth()
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const classes = useStyles()

  return (
    <Box>
      <Grid
        container
        direction='column'
        justify='center'
        alignItems='center'
        className={classes.screen}
      >
        <Backdrop style={{ zIndex: 10 }} open={loading}>
          <CircularProgress color='inherit' />
        </Backdrop>
        <Fade direction="up" in={true} timeout={1000} mountOnEnter unmountOnExit>
        <Card elevation={3}>
            <Box p={3} className={classes.card}>
              <Grid
                container
                direction='column'
                justify='center'
                alignItems='center'
              >
                <Grid item className={classes.gridItem}>
                  <Box className={classes.logo}>
                    <Typography variant='h2' color='secondary'><b>MPF</b></Typography>
                  </Box>
                </Grid>
                <Grid item className={classes.gridItem}>
                  {error && error.non_field_errors
                    ? <Box my={3}><Alert severity='error' variant='filled'>{error.non_field_errors}</Alert></Box>
                    : null}
                  <Grid
                    container
                    direction='column'
                    justify='center'
                    alignItems='stretch'
                  >
                    <TextFieldLight
                      className={classes.gridItem}
                      label={_('Username')}
                      value={username}
                      onChange={(e) => setUsername(e.target.value)}
                      onKeyPress={(event) => {
                        if (event.key === 'Enter') login(username, password)
                      }}
                      error={error && error.username}
                      helperText={error && error.username ? error.username : ''}
                      InputProps={{
                        style: {
                          color: 'white'
                        }
                      }}
                    />
                    <TextFieldLight
                      label={_('Password')}
                      type='password'
                      value={password}
                      error={error && error.password}
                      helperText={error && error.password ? error.password : ''}
                      onChange={(e) => setPassword(e.target.value)}
                      onKeyPress={(event) => {
                        if (event.key === 'Enter') login(username, password)
                      }}
                      style={{
                        borderBottom: 'white'
                      }}
                      InputProps={{
                        style: {
                          color: 'white'
                        }
                      }}
                    />
                    <br />
                    <Button
                      width={1}
                      variant='contained'
                      color='secondary.main'
                      my={3}
                      onClick={() => login(username, password)}
                    >{_('Login')}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Box>
          </Card>
        </Fade>

      </Grid>
    </Box>
  )
}

export default LoginScreen
