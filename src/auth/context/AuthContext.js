import React, { useState, useEffect } from 'react'
import axios from 'axios'
import config from 'config.js'
import http from 'core/http'
const AuthContext = React.createContext()

export function AuthProvider (props) {
  const { children } = props

  const initialState = {
    initialized: false,
    loading: false,
    error: null,
    loggedIn: false,
    token: null,
    user: {
      firstName: 'lkasdhgas',
      lastName: 'kajshgs'
    }
  }
  const [state, setState] = useState(initialState)

  useEffect(() => {
    if (state.initialized) {
      if (state.loggedIn) window.localStorage.setItem('token', state.token)
      else window.localStorage.removeItem('token')
    } else {
      const token = window.localStorage.getItem('token')
      if (token) {
        setState({
          ...state,
          initialized: true,
          loggedIn: false,
          token
        })
      } else {
        setState({
          ...state,
          initialized: true
        })
      }
    }
  }, [state])

  const value = {
    ...state,
    login: async (username, password) => {
      setState({
        ...state,
        loading: true,
        error: null
      })
      try {
        const result = await axios({
          baseURL: config.apiBaseUrl,
          url: '/auth/login',
          method: 'post',
          data: {
            username: username,
            password: password
          }
        })
        setState({
          ...state,
          loading: false,
          loggedIn: true,
          user: result.data.user,
          token: result.data.token
        })
      } catch (e) {
        setState({
          ...state,
          loading: false,
          error: e.response.data
        })
      }
    },
    logout: () => {
      setState({
        ...state,
        loggedIn: false,
        token: null
      })
    }
  }

  if (!state.initialized) {
    return null
  }

  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  )
}

export default AuthContext
