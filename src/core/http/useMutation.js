import { useState, useMemo, useCallback } from 'react'
import { useNotification } from 'core/notification'
import useHttp from './useHttp'

function useMutation (action, endpoint, config = {}) {
  const http = useHttp()
  const notification = useNotification()

  const [state, setState] = useState({
    loading: false,
    validationError: null
  })

  const requests = {
    PATCH: http.patch,
    POST: http.post,
    PUT: http.put
  }
  const submit = useCallback(async (data) => {
    console.trace('useMutation', 'submit', { action, endpoint, data })
    setState({
      loading: true,
      validationError: null
    })

    const request = requests[action]

    try {
      const res = await request(endpoint, data)

      notification.http(res.status)
      if (config.onSuccess) {
        config.onSuccess()
      }
      setState({
        loading: false,
        validationError: null
      })
    } catch (e) {
      if (!e.response) {
        throw e
      }
      notification.http(e.response.status)
      setState({
        loading: false,
        validationError: e.response.status === 400
          ? e.response.data
          : null
      })
    }
  }, [setState])

  const response = useMemo(() => [
    submit,
    state
  ], [submit, state])

  return response
}

export default useMutation
