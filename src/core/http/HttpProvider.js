import React, { useMemo } from 'react'
import axios from 'axios'
import config from 'config'
import { useAuth } from 'auth/hooks'
import { useLocale } from 'locale'
import { sleep } from 'core/utils'
import HttpContext from './HttpContext'
import requestCounter from './requestCounter'

const holdQuery = 0

function HttpProvider (props) {
  const { children } = props

  const { loggedIn, token } = useAuth()
  const { language } = useLocale()

  const http = useMemo(() => {
    const instance = axios.create({
      baseURL: config.apiBaseUrl,
      headers: {
        ...(loggedIn
          ? { Authorization: `Token ${token}` }
          : {}
        ),
        'Accept-Language': language
      }
    })

    return {
      get: async (url, config) => {
        console.trace('useHttp', 'GET', { url, config })
        requestCounter.notifyStart()
        if (holdQuery) {
          await sleep(holdQuery)
        }
        try {
          const res = await instance.get(url, config)
          return res
        } finally {
          requestCounter.notifyFinish()
        }
      },
      delete: async (url, config) => {
        console.trace('useHttp', 'DELETE', { url, config })
        requestCounter.notifyStart()
        try {
          const res = await instance.delete(url, config)
          return res
        } finally {
          requestCounter.notifyFinish()
        }
      },
      head: async (url, config) => {
        console.trace('useHttp', 'HEAD', { url, config })
        requestCounter.notifyStart()
        try {
          const res = await instance.head(url, config)
        } finally {
          requestCounter.notifyFinish()
        }
      },
      options: async (url, config) => {
        console.trace('useHttp', 'OPTIONS', { url, config })
        requestCounter.notifyStart()
        try {
          const res = await instance.options(url, config)
          return res
        } finally {
          requestCounter.notifyFinish()
        }
      },
      post: async (url, data, config) => {
        console.trace('useHttp', 'POST', { url, data, config })
        requestCounter.notifyStart()
        try {
          const res = await instance.post(url, data, config)
          return res
        } finally {
          requestCounter.notifyFinish()
        }
      },
      put: async (url, data, config) => {
        console.trace('useHttp', 'PUT', { url, data, config })
        requestCounter.notifyStart()
        try {
          const res = await instance.put(url, data, config)
          return res
        } finally {
          requestCounter.notifyFinish()
        }
      },
      patch: async (url, data, config) => {
        console.trace('useHttp', 'PATCH', { url, data, config })
        requestCounter.notifyStart()
        try {
          const res = await instance.patch(url, data, config)
          return res
        } finally {
          requestCounter.notifyFinish()
        }
      }
    }
  }, [language, loggedIn, token])

  return (
    <HttpContext.Provider value={http}>
      {children}
    </HttpContext.Provider>
  )
}

export default HttpProvider
