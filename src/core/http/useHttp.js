import { useContext } from 'react'
import HttpContext from './HttpContext'

function useHttp () {
  return useContext(HttpContext)
}

export default useHttp
