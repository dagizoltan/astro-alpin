import { useState, useEffect } from 'react'
import * as R from 'ramda'
import { useScreen } from 'core/screen'
import useHttp from './useHttp'

function useQuery (urlOrFetchFunction, config) {
  const http = useHttp()
  const screen = useScreen()
  const isActive = screen ? screen.isActive : false

  const [state, setState] = useState({
    initialized: false,
    loading: false,
    error: null,
    data: undefined
  })

  const [refetchCounter, setRefetchCounter] = useState(0)

  useEffect(() => {
    (async () => {
      if (!urlOrFetchFunction) {
        return
      }
      console.trace('useQuery', 'effect', { urlOrFetchFunction, config })
      const fullReloadExtraState = R.defaultTo(false, R.prop('fullReload', config))
        ? { initialized: false }
        : {}
      setState({
        ...state,
        ...fullReloadExtraState,
        loading: true,
        error: null
      })
      try {
        if (typeof urlOrFetchFunction === 'function') {
          const data = await urlOrFetchFunction(config)
          setState({
            initialized: true,
            loading: false,
            error: null,
            data: data
          })
        } else {
          const method = R.defaultTo('GET', R.prop('method', config))
          const reqConfig = R.dissoc('method', config)
          const res = method === 'OPTIONS'
            ? await http.options(urlOrFetchFunction, reqConfig)
            : await http.get(urlOrFetchFunction, reqConfig)

          setState({
            initialized: true,
            loading: false,
            error: null,
            data: res.data
          })
        }
      } catch (e) {
        setState({
          initialized: false,
          loading: false,
          error: e,
          data: undefined
        })
      }
    })()
  }, [urlOrFetchFunction, config, isActive, refetchCounter])

  return {
    ...state,
    refetch: () => setRefetchCounter(refetchCounter + 1)
  }
}

export default useQuery
