
const state = {
  requestCount: 0,
  subscriptions: []
}

export default {
  notifyStart: () => {
    state.requestCount++
    state.subscriptions.forEach((x) => {
      x(state.requestCount)
    })
  },
  notifyFinish: () => {
    state.requestCount--
    state.subscriptions.forEach((x) => {
      x(state.requestCount)
    })
  },
  subscribe: (cb) => {
    state.subscriptions.push(cb)
    return () => {
      state.subscriptions = state.subscriptions.filter((x) => x !== cb)
    }
  }
}
