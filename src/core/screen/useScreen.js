import { useContext } from 'react'
import ScreenContext from './ScreenContext'

function useScreen () {
  return useContext(ScreenContext)
}

export default useScreen
