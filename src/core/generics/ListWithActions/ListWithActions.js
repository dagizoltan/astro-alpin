import React, { useMemo } from 'react'
import List from '../Table'
import { Link } from 'navigation/components'
import { useLocale } from 'locale'
import { Box, Button, Grid } from '@material-ui/core'

import SearchBox from '../websocket/SearchBox'
import useCrud from './useCrud'

function ListWithActions (props) {

  const { route, hideActions, actions } = props
  const { _ } = useLocale()

  const crud = useCrud({ module: route })

  const finalActions = useMemo(() =>
    hideActions
      ? [...actions]
      : [
        ...actions,
        ...crud
      ], [actions, hideActions])

  return (
    <>
      <Box mb={1}>
        <Grid
          container
          direction='row'
          justify='space-between'
          alignItems='center'
        >
          <Grid item>
            <Link to={route + '/create'}>
              <Button size='small' variant='contained' color='primary'>{_('Add new')}</Button>
            </Link>
          </Grid>
          <Grid item>
            <SearchBox />
          </Grid>
        </Grid>
      </Box>
      <List {...props} actions={finalActions} />
    </>
  )
}

ListWithActions.defaultProps = {
  actions: []
}

export default ListWithActions
