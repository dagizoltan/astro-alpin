import React, { useEffect } from 'react'
import { useNavigation } from 'navigation/hooks'
import { useNotification } from 'core/notification'
import RemoveSnackBar from './RemoveSnackBar'

import { Slide } from '@material-ui/core'
import { Edit as EditIcon, Delete as DeleteIcon, Visibility as ViewIcon } from '@material-ui/icons'

function useCrud (props) {
  const {
    module
  } = props

  const { navigate } = useNavigation()

  const { customNotification, closeSnackbar } = useNotification()


  const crud = [
    {
      icon: ViewIcon,
      handler: (row) => {
        navigate(module + '/' + row._id)
      }
    },
    {
      icon: EditIcon,
      handler: (row) => {
        navigate(module + '/' + row._id + '/edit')
      }
    },
    {
      icon: DeleteIcon,
      handler: (row) => {
        const notificationConfig = {
          variant: 'warning',
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right'
          },
          TransitionComponent: (props) => <Slide {...props} direction='left' />,
          action: (key) => <RemoveSnackBar key={key} row={row} module={module} closeSnackbar={closeSnackbar} />,
          persist: true,
          preventDuplicate: true
        }
        customNotification(
          `Are you sure you want to delete record ${row._id}?`,
          notificationConfig
        )
      }
    }
  ]

  return crud
}

export default useCrud
