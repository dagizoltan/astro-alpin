import React, { useEffect } from 'react'

import { useMutation } from 'core/websocket'

import { Button } from '@material-ui/core'

function RemoveSnackBar (props) {
  const {
    key,
    row,
    module,
    closeSnackbar
  } = props

  const [mutate, mutationListener] = useMutation({
    method: 'remove',
    module,
    config: { onSuccess: () => { closeSnackbar(key) } }
  })

  useEffect(mutationListener)

  return (
    <>
      <Button size='small' onClick={() => { mutate({ id: row._id }) }}>
                Yes
      </Button>
      <Button size='small' onClick={() => { closeSnackbar(key) }}>
                No
      </Button>
    </>
  )
}

export default RemoveSnackBar
