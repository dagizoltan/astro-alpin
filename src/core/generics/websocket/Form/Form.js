// @ts-nocheck
import React, { useCallback, useEffect, useState } from 'react'
import { Box, Button, ButtonGroup, Card, Grid } from '@material-ui/core'
import { useQuery, useMutation } from 'core/websocket'
import { LoadingIndicator } from 'core/ui'
import FormTemplate from 'core/ui/Form'

import { useLocale } from 'locale'
import { deepCleanEmptyValues } from 'core/utils'
import { useNavigation } from 'navigation/hooks'

function Form (props) {
  const { module, method, initialData, validationError } = props

  const [formData, setFormData] = useState(initialData)

  const { _ } = useLocale()
  const { popTo } = useNavigation()

  const { data, initialized } = useQuery({
    module: module + '/forms',
    queryConfig: { method }
  })

  const [mutate, mutationListener, { loading }] = useMutation({ module, method, config: { onSuccess: () => { popTo(module) } } })

  const cleanSubmit = useCallback((data) => {
    mutate(deepCleanEmptyValues(data))
  }, [])

  useEffect(mutationListener)

  if (!initialized) {
    return <LoadingIndicator />
  }

  return (
    <Box mb={3}>
      <Card>
        <Grid
          container
          direction='column'
          justify='flex-start'
          alignItems='stretch'
        >
          <Box p={3}>
            <FormTemplate
              schema={data.schema}
              value={formData}
              onChange={setFormData}
              error={validationError}
            />
            <Box mt={1}>
              <Grid
                container
                direction='row'
                justify='flex-end'
                alignItems='center'
              >
                <ButtonGroup size='small'>
                  <Button
                    color='primary'
                    variant='contained'
                    onClick={() => cleanSubmit(formData)}
                  >
                    {_('Submit')}
                    <LoadingIndicator loading={loading} />
                  </Button>
                </ButtonGroup>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Card>
    </Box>
  )
}

export default Form
