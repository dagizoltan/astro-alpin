import React from 'react'
import SearchBox from './SearchBox'
import ProviderGroup from 'ProviderGroup.js'
export default { title: 'layout.SearchBox' }

export const normal = () =>
  <ProviderGroup>
    <SearchBox />
  </ProviderGroup>
