// *https://www.registers.service.gov.uk/registers/country/use-the-api*
import React, { useEffect, useMemo, useState } from 'react'
import * as R from 'ramda'
import { Box, CircularProgress, InputBase, Typography } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'

import { Search as SearchIcon } from '@material-ui/icons'
import { useList } from 'core/websocket'
import { Link } from 'navigation/components'
import useStyles from './useStyles'

function SearchBox (props) {
  const {
    module = '/search',
    models
  } = props

  const classes = useStyles()

  const [phrase, setPhrase] = useState('')
  const [open, setOpen] = React.useState(false)

  const queryConfig = useMemo(() => ({
    filter: {
      phrase,
      models
    },
    params: R.mergeAll([
      { limit: 25 },
      { page: 1 }
    ])
  }), [phrase])

  const { data, initialized, fetch } = useList({ route: module, queryConfig })

  const options = useMemo(() => data ? data : [])
  return (
    <Autocomplete
      open={open}
      onOpen={() => {
        setOpen(true)
      }}
      onClose={() => {
        setOpen(false)
      }}
      getOptionSelected={(option, value) => option.name === value.name}
      getOptionLabel={(option) => option.name}
      options={options}
      loading={!initialized}

      renderInput={(params) => (
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            onChange={e => setPhrase(e.target.value)}
            variant='outlined'
            size='small'
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput
            }}
            {...params}
            ref={params.InputProps.ref}
            label='Search'
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  {!initialized ? <CircularProgress color='inherit' size={20} /> : null}
                  {params.InputProps.endAdornment}
                </>
              )
            }}
          />
        </div>
      )}
      renderOption={(option) => (
        <Link to={option.link} reset>
          <Typography variant='h4'>{option.name}</Typography>
          <Typography variant='caption'>{option.module}</Typography>
        </Link>
      )}
    />
  )
}

export default SearchBox
