import React, { useState, useEffect } from 'react'
import * as R from 'ramda'
import { useMediaQuery, Box, Card } from '@material-ui/core'
import FilterListIcon from '@material-ui/icons/FilterList'
import { useTheme } from '@material-ui/core/styles'

import ApiForm from './rest/ApiForm'
import { useLocale } from 'locale'

const getFilledValues = R.pickBy(
  R.complement(
    R.anyPass([R.isEmpty, R.isNil])
  )
)

function Filter (props) {
  const { endpoint, fields, value, onChange, validationError } = props
  const { _ } = useLocale()
  const theme = useTheme()
  const shouldAutoOpen = useMediaQuery(theme.breakpoints.up('md'))
  const [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    setIsOpen(shouldAutoOpen)
  }, [shouldAutoOpen])

  return (
    <Card
      expanded={isOpen}
      onExpandChange={(e, expanded) => setIsOpen(expanded)}
      icon={<FilterListIcon />}
      title={_('Filter')}
    >
      <Box py={1} px={3}>
        <Box fontWeight='fontWeightBold'>Szürö</Box>
        <ApiForm
          endpoint={endpoint}
          fields={fields}
          initialData={value}
          validationError={validationError}
          onSubmit={(x) => onChange(getFilledValues(x))}
          hideLabels
        />
      </Box>
    </Card>
  )
}

export default Filter
