import React, { useState, useMemo } from 'react'
import * as R from 'ramda'
import { useLocale } from 'locale'
import { Filter } from 'core/generics'
import { Table as TableUI, TableCell, LoadingIndicator } from 'core/ui'
import { useList } from 'core/websocket'
import { Grid } from '@material-ui/core'

function Table (props) {
  const {
    route, fields, orderingFields, filterFields, onRowPress,
    actions, resolveField, hideFilters = true, children, defaultFilter
  } = props

  const { _ } = useLocale()

  const [orderField, setOrderField] = useState(null)
  const [orderDirection, setOrderDirection] = useState('asc')
  const [pageSize, setPageSize] = useState(10)
  const [page, setPage] = useState(1)
  const [filter, setFilter] = useState(R.defaultTo(undefined, defaultFilter))

  const queryConfig = useMemo(() => ({
    filter,
    params: R.mergeAll([
      { limit: pageSize },
      { page: page },
      { sort: (orderDirection === 'desc' ? '-' : '') + orderField }
    ])
  }), [filter, pageSize, page, orderDirection, orderField])

  const { data, initialized } = useList({ route, queryConfig })

  if (!initialized) {
    return <LoadingIndicator />
  }

  const tableColumns = fields.map((x) => {
    return {
      id: x,
      Header: (x !== '_id') ? _(x.charAt(0).toUpperCase() + x.slice(1).replace(/([A-Z])/g, ' $1')) : 'ID',
      accessor: (row) => resolveField(x, row),
      Cell: TableCell,
      disableSortBy: orderingFields === undefined
        ? false
        : !R.contains(x, orderingFields)
    }
  })

  const tableUI = (
    <TableUI
      columns={tableColumns}
      data={data.docs}
      page={page}
      setPage={setPage}
      pageSize={pageSize}
      setPageSize={setPageSize}
      orderField={orderField}
      setOrderField={setOrderField}
      orderDirection={orderDirection}
      setOrderDirection={setOrderDirection}
      rowCount={data.totalDocs}
      onRowPress={onRowPress}
      actions={actions}
      loading={!initialized}
    />
  )
  const filterUI = (
    <Filter
      endpoint={route}
      fields={filterFields || fields}
      value={filter}
      onChange={(x) => {
        setPage(1)
        setFilter(x)
      }}
    />
  )
  return (
    <Grid container spacing={3} direction='row-reverse'>
      {!hideFilters
        ? (
          <Grid item xs={12} lg={3}>
            {filterUI}
          </Grid>
        ) : null}
      <Grid item xs={12} lg={hideFilters ? 12 : 9}>
        {tableUI}
        {children}
      </Grid>
    </Grid>
  )
}

Table.defaultProps = {
  route: null,
  fields: [],
  orderingFields: undefined,
  filterFields: undefined,
  onRowPress: null,
  hideFilter: false,
  resolveField: (x, row) => row[x]
}

export default Table
