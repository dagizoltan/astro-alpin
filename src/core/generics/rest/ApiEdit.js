import React, { useMemo, useEffect } from 'react'
import EditIcon from '@material-ui/icons/Edit'
import { Card } from '@material-ui/core'
import { LoadingIndicator, ErrorIndicator } from 'core/ui'
import { useQuery, useMutation } from 'core/http'
import { useNavigation } from 'navigation/hooks'
import ApiForm from './ApiForm'

function ApiEdit (props) {
  const { endpoint, action, hasDeleteWidget, listRoute, SubmitBar, fields } = props

  const { popTo, setTitle } = useNavigation()

  const queryInput = useMemo(() =>
    action === 'POST'
      ? () => ({})
      : endpoint
  , [action, endpoint])
  const { data, initialized, error, refetch } = useQuery(queryInput)

  const [submitForm, { validationError, loading: submitting }] = useMutation(action, endpoint, { onSuccess: () => { popTo(listRoute) } })

  useEffect(() => {
    if (data) {
      setTitle(data.name)
    }
  }, [data])

  if (error) return <ErrorIndicator error={error} retry={refetch} />
  if (!initialized) return <LoadingIndicator />

  return (
    <LoadingIndicator loading={submitting}>
      <Card>
        <ApiForm
          hasDeleteWidget={hasDeleteWidget}
          listRoute={listRoute}
          endpoint={endpoint}
          initialData={data}
          action={action}
          fields={fields}
          onSubmit={submitForm}
          SubmitBar={SubmitBar}
          validationError={validationError}
        />
      </Card>
    </LoadingIndicator>
  )
}

export default React.memo(ApiEdit)
