import React from 'react'
import ApiEdit from './ApiEdit'

function ApiCreateEdit (props) {
  const { endpoint, id, hasDeleteWidget, listRoute, SubmitBar, fields } = props

  return (
    <ApiEdit
      endpoint={id ? endpoint + id + '/' : endpoint}
      action={id ? 'PUT' : 'POST'}
      listRoute={listRoute}
      fields={fields}
      hasDeleteWidget={hasDeleteWidget}
      SubmitBar={SubmitBar}
    />
  )
}

export default React.memo(ApiCreateEdit)
