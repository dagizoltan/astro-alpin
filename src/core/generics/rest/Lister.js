import React, { useState, useMemo } from 'react'
import * as R from 'ramda'
import { useLocale } from 'locale'
import { Filter } from 'core/generics'
import { Table, TableCell, LoadingIndicator } from 'core/ui'
import { useDetail } from 'core/websocket'
import { Grid } from '@material-ui/core'

function Lister (props) {
  const {
    endpoint, fields, orderingFields, filterFields, onRowPress,
    actions, hideFilter, resolveField, children
  } = props

  const { _ } = useLocale()
  const [orderField, setOrderField] = useState(null)
  const [orderDirection, setOrderDirection] = useState('asc')
  const [pageSize, setPageSize] = useState(10)
  const [page, setPage] = useState(1)
  const [filter, setFilter] = useState({})

  const filterParams = filter ? R.mapObjIndexed((f) => Array.isArray(f) ? '[' + f.map(x => x.id).toString() + ']' : typeof f === 'object' ? f.id : f, filter) : {}

  const queryConfig = useMemo(() => ({

    params: R.mergeAll([
      filterParams,
      { pagesize: pageSize },
      { page: page },
      { ordering: (orderDirection === 'desc' ? '-' : '') + orderField }
    ])
  }), [filter, pageSize, page, orderDirection, orderField])
  const { data, initialized } = useDetail(endpoint, queryConfig)

  /* TODO: errorHandling
  if (error) {
    return <ErrorIndicator error={error} retry={refetch} />
  }
  */

  if (!initialized) {
    return <LoadingIndicator />
  }

  const tableColumns = fields.map((x) => {
    return {
      id: x,
      Header: _(x.charAt(0).toUpperCase() + x.slice(1).replace(/([A-Z])/g, ' $1')),
      accessor: (row) => resolveField(x, row),
      Cell: TableCell,
      disableSortBy: orderingFields === undefined
        ? false
        : !R.contains(x, orderingFields)
    }
  })

  const tableUI = (
    <Table
      columns={tableColumns}
      data={data.docs}
      page={page}
      setPage={setPage}
      pageSize={data.limit}
      setPageSize={setPageSize}
      orderField={orderField}
      setOrderField={setOrderField}
      orderDirection={orderDirection}
      setOrderDirection={setOrderDirection}
      rowCount={data.totalDocs}
      onRowPress={onRowPress}
      actions={actions}
      // loading={loading}
    />
  )
  const filterUI = (
    <Filter
      endpoint={endpoint}
      fields={filterFields || fields}
      value={filter}
      onChange={(x) => {
        setPage(1)
        setFilter(x)
      }}
    />
  )
  return (
    <Grid container spacing={3} direction='row-reverse'>
      {!hideFilter
        ? (
          <Grid item xs={12} lg={3}>
            {filterUI}
          </Grid>
        ) : null}
      <Grid item xs={12} lg={hideFilter ? 12 : 9}>
        {tableUI}
        {children}
      </Grid>
    </Grid>
  )
}

Lister.defaultProps = {
  endpoint: null,
  fields: [],
  orderingFields: undefined,
  filterFields: undefined,
  onRowPress: null,
  hideFilter: false,
  resolveField: (x, row) => row[x]
}

export default Lister
