import React, { useMemo } from 'react'
import Lister from './Lister'
import { Link } from 'navigation/components'
import { useLocale } from 'locale'
import { Button } from '@material-ui/core'
import { Edit as EditIcon, Visibility as ViewIcon } from '@material-ui/icons'
import { useNavigation } from 'navigation/hooks'

function ApiCrud (props) {
  const { baseUrl, hideActions, actions } = props
  const { _ } = useLocale()
  const { navigate } = useNavigation()
  const finalActions = useMemo(() =>
    hideActions
      ? [...actions]
      : [
        ...actions,
        {
          icon: ViewIcon,
          action: (row) => {
            navigate(baseUrl + '/' + row._id)
          }
        },
        {
          icon: EditIcon,
          action: (row) => {
            navigate(baseUrl + '/' + row._id + '/edit')
          }
        }
      ]
  , [actions, hideActions])

  return (
    <Lister
      {...props}
      actions={finalActions}
    >
      <Link to={baseUrl + '/create'}>
        <Button variant='contained' color='primary'>{_('Add new')}</Button>
      </Link>
    </Lister>
  )
}

ApiCrud.defaultProps = {
  actions: []
}

export default ApiCrud
