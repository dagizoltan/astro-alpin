// @ts-nocheck
import React, { useCallback, useState, useMemo } from 'react'
import * as R from 'ramda'
import { Box, Button, Grid, ButtonGroup } from '@material-ui/core'
import { useQuery } from 'core/http'
import { Form, LoadingIndicator, ErrorIndicator } from 'core/ui'
import { useLocale } from 'locale'
import { deepCleanEmptyValues } from 'core/utils'
import { makeStyles } from '@material-ui/core/styles'

const optionsQueryConfig = { method: 'OPTIONS' }

const getReadOnlyKeys = R.pipe(
  R.mapObjIndexed(R.prop('readOnly')),
  R.toPairs,
  R.filter(R.propEq(1, true)),
  R.map(R.prop(0))
)

function ApiForm (props) {
  const { endpoint, fields, action, initialData, isLocked, listRoute, onSubmit, validationError, hideLabels, hasDeleteWidget, SubmitBar } = props
  const [formData, setFormData] = useState(initialData)
  const classes = useStyles()
  const { _ } = useLocale()

  const { data, initialized, error, refetch } = useQuery(endpoint, optionsQueryConfig)
  const schema = useMemo(() => {
    if (!initialized) {
      return null
    }
    const finalSchema = data.schema
    const actionFields = action
      ? R.path(['actions', action], data)
      : null
    // const readOnlyKeys = getReadOnlyKeys(actionFields)

    return finalSchema
  }, [data])

  const cleanSubmit = useCallback((data) => {
    onSubmit(deepCleanEmptyValues(data))
  }, [])

  if (error) {
    return <ErrorIndicator error={error} retry={refetch} />
  }

  if (!initialized) {
    return <LoadingIndicator />
  }
  return (
    <Grid
      container
      direction='column'
      justify='flex-start'
      alignItems='stretch'
    >
      <Box p={3}>
        <Form
          schema={schema}
          value={formData}
          onChange={setFormData}
          error={validationError}
        />
        <ButtonGroup className={classes.buttonContainer} disabled={isLocked}>
          {
            SubmitBar
              ? (
                <SubmitBar formData={formData} submitForm={cleanSubmit} setFormData={setFormData} />
              ) : (
                <Button
                  color='primary'
                  variant='contained'
                  onClick={() => cleanSubmit(formData)}
                >
                  {_('Submit')}
                </Button>
              )
          }
        </ButtonGroup>
      </Box>
    </Grid>
  )
}

const useStyles = makeStyles(theme => ({
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
}))

export default React.memo(ApiForm)
