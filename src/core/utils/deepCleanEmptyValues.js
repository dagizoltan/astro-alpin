import * as R from 'ramda'

const deepCleanEmptyValues = (data) => R.cond([
  [
    R.is(Array),
    R.pipe(
      R.map(deepCleanEmptyValues),
      R.filter(R.complement(R.either(R.isNil, R.isEmpty)))
    )
  ],
  [
    R.is(Object),
    R.pipe(
      R.map(deepCleanEmptyValues),
      R.toPairs,
      R.filter(R.propSatisfies(R.complement(R.either(R.isNil, R.isEmpty)), 1)),
      R.fromPairs
    )],
  [
    R.T,
    R.identity
  ]
])(data)

export default deepCleanEmptyValues
