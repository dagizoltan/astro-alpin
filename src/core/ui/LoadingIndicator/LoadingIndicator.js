import React from 'react'
import { Box, CircularProgress } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'

function LoadingIndicator (props) {
  const { loading, children } = props
  const classes = useStyles({ loading })

  if (children) {
    return (
      <Box className={classes.container}>
        {children}
        <Box className={classes.overlay}>
          <CircularProgress />
        </Box>
      </Box>
    )
  }

  if (loading) {
    return (
      <Box className={classes.containerWithoutChildren}>
        <CircularProgress />
      </Box>
    )
  }
  return null
}

LoadingIndicator.defaultProps = {
  loading: true
}

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative'
  },
  containerWithoutChildren: {
    display: 'flex',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    display: 'flex',
    pointerEvents: ({ loading }) => loading ? 'inherit' : 'none',
    opacity: ({ loading }) => loading ? 1 : 0,
    transition: 'opacity 1s ease',
    alignItems: 'center',
    justifyContent: 'center'
  }
}))

export default LoadingIndicator
