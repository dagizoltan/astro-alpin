import React from 'react'
import { Grid } from '@material-ui/core'
import { Field } from './fields'

function Form (props) {
  const { schema, value, onChange, error } = props
  return (
    <Grid
      container
      direction='column'
      justify='flex-start'
      alignItems='stretch'
    >
      <Field
        schema={schema}
        value={value}
        onChange={onChange}
        error={error}
      />
    </Grid>
  )
}


export default Form
