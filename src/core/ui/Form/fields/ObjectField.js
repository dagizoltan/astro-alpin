import React, { Fragment } from 'react'
import * as R from 'ramda'
import Field from './Field'
import { Box, Grid, Hidden } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { EditorJSWidget, FormLabel } from '../widgets'

const widgetTypes = {
  editorjs: EditorJSWidget
}

function ObjectField (props) {
  const { schema, value, onChange, error } = props
  const classes = useStyles()

  const hideLabels = R.path(['ui', 'hideLabels'], schema)
  const widget = R.path(['ui', 'ui:widget'], schema)

  if (widgetTypes[widget]) {
    const Widget = widgetTypes[widget]
    const ui = R.prop('ui', schema)

    return (
      <Widget
        ui={ui}
        value={value}
        onChange={onChange}
        error={error}
        required={schema.required}
        readOnly={schema.readOnly}
      />
    )
  }

  if (widget === 'inlineObject') {
    if (schema.properties) {
      const entries = Object.entries(schema.properties)
      return (
        <Grid container>
          {entries.map(([k, v], i) =>
            <Box item key={i} width={1 / 4}>
              <Box className={classes.container}>
                <Hidden smDown>
                  {!hideLabels
                    ? <FormLabel required={v.required} label={v.title} />
                    : null}
                </Hidden>
                <Box>
                  <Field
                    schema={R.set(R.lensPath(['ui', 'ui:label']), v.title, v)}
                    value={R.prop(k, value)}
                    onChange={(x) => onChange(R.assoc(k, x, R.defaultTo({}, value)))}
                    error={R.prop(k, error)}
                  />
                </Box>
              </Box>
              {v.type === 'object' && i < entries.length - 1
                ? <Box flex='1' borderBottom='1px solid #eee' mb='10px' />
                : null}
            </Box>
          )}
        </Grid>
      )
    }
    return null
  }

  if (schema.properties) {
    const entries = Object.entries(schema.properties)
    return (
      <Box flex='1'>
        {entries.map(([k, v], i) =>
          <Fragment key={i}>
            <Box className={classes.container}>
              <Box flex='1'>
                <Field
                  schema={v}
                  value={R.prop(k, value)}
                  onChange={(x) => onChange(R.assoc(k, x, R.defaultTo({}, value)))}
                  error={R.prop(k, error)}
                />
              </Box>
            </Box>
            {v.type === 'object' && i < entries.length - 1
              ? <Box flex='1' borderBottom='1px solid #eee' mb='10px' />
              : null}
          </Fragment>
        )}
      </Box>
    )
  }
  return null
}

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center'
  }
}))

export default ObjectField


/*
  <Hidden smDown>
    {!hideLabels
      ? <FormLabel required={v.required} label={v.ui['ui:label']} />
      : null}
  </Hidden>
*/
