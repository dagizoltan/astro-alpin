import React from 'react'
import * as R from 'ramda'
import { BooleanWidget } from '../widgets'

function BooleanField (props) {
  const { schema, value, onChange } = props

  const ui = R.prop('ui', schema)
  const defaultValue = R.defaultTo(false, R.prop('default', schema))
  return (
    <BooleanWidget
      ui={ui}
      value={R.defaultTo(defaultValue, value)}
      onChange={onChange}
      required={schema.required}
      readOnly={schema.readOnly}
    />
  )
}

export default BooleanField
