import React from 'react'
import * as R from 'ramda'
import Field from './Field'
import { useLocale } from 'locale'
import { Box, Button, Typography, ExpansionPanel, ExpansionPanelDetails, ExpansionPanelActions } from '@material-ui/core'
import FormErrorWidget from '../widgets/FormErrorWidget'
import { ForeignKeyWidget } from '../widgets'

function ArrayField (props) {
  const { schema, value, onChange, error } = props
  const { _ } = useLocale()
  const ui = R.prop('ui', schema)

  if (schema.items.type !== 'object') {
    return (
      <>
        <ForeignKeyWidget
          multiple
          ui={ui}
          schema={schema}
          value={R.defaultTo([], value)}
          onChange={onChange}
          type='number'
          required={schema.required}
          readOnly={schema.readOnly}
          error={error}
        />
        <FormErrorWidget errors={error} />
      </>
    )
  }

  return (
    <>
      <Box>
        <Typography variant='caption' color={error ? 'secondary' : undefined}>{schema.title}</Typography>
        <br />
        {R.defaultTo([], value).map((x, i) =>
          <ExpansionPanel key={i} defaultExpanded>
            <ExpansionPanelDetails>
              <Field
                schema={schema.items}
                value={x}
                onChange={(v) => onChange(R.update(i, v, value))}
                error={R.prop(i, error)}
              />
            </ExpansionPanelDetails>
            <ExpansionPanelActions>
              <Button color='secondary' onClick={() => onChange(R.remove(i, 1, value))}>Törlés</Button>
            </ExpansionPanelActions>
          </ExpansionPanel>
        )}
        <Button
          color='primary'
          variant='contained'
          size='small'
          onClick={() =>
            onChange(R.append(undefined, R.defaultTo([], value)))}
        >
          {_('Add new')}
        </Button>
      </Box>
    </>
  )
}

export default ArrayField
