import React, { useMemo } from 'react'
import { Alert } from '@material-ui/lab'

import ObjectField from './ObjectField'
import StringField from './StringField'
import NumberField from './NumberField'
import BooleanField from './BooleanField'
import ArrayField from './ArrayField'

function Field (props) {
  const { schema } = props

  const types = {
    object: ObjectField,
    string: StringField,
    number: NumberField,
    boolean: BooleanField,
    array: ArrayField
  }

  if (types[schema.type]) {
    const TypeComponent = types[schema.type]

    return (
      <TypeComponent
        {...props}
      />
    )
  }
  return <Alert severity='error' variant='outlined'>Invalid field type '{schema.type}'</Alert>
}

export default Field
