import React from 'react'
import * as R from 'ramda'
import {
  SelectWidget,
  DateWidget,
  DateTimeWidget,
  ForeignKeyWidget,
  TextInputWidget,
  MultiSelectWidget,
  ImageUploadWidget
} from '../widgets'

const widgetTypes = {
  email: TextInputWidget,
  foreignkey: ForeignKeyWidget,
  password: TextInputWidget,
  choice: SelectWidget,
  'multiple choice': MultiSelectWidget,
  imageUpload: ImageUploadWidget,
  textarea: TextInputWidget,
  date: DateWidget,
  'date-time': DateTimeWidget,
  default: TextInputWidget
}

function StringField (props) {
  const { schema, value, onChange, error } = props
  const ui = R.prop('ui', schema)
  const defaultValue = R.defaultTo('', R.prop('default', schema))

  if (['choice', 'multiple choice'].indexOf(schema.actionType) > -1) {
    schema.ui['ui:widget'] = schema.actionType
    schema.ui['ui:choices'] = []
    if (schema.choices) schema.choices.map(({ value, displayName }) => schema.ui['ui:choices'].push({ value, displayName }))
  }
  const Widget = R.defaultTo(
    widgetTypes.default,
    R.prop(R.prop('ui:widget', ui), widgetTypes)
  )
  return (
    <Widget
      ui={ui}
      value={R.defaultTo(defaultValue, value)}
      onChange={onChange}
      error={error}
      required={schema.required}
      readOnly={schema.readOnly}
    />
  )
}

export default StringField
