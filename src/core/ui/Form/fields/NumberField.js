import React from 'react'
import * as R from 'ramda'
import { 
  ForeignKeyWidget,
  TextInputWidget
} from '../widgets'

const widgetTypes = {
  foreignkey: ForeignKeyWidget,
  default: TextInputWidget
}

function NumberField (props) {
  const { schema, value, onChange, error } = props
  const ui = R.prop('ui', schema)
  const defaultValue = R.defaultTo('', R.prop('default', schema))

  const Widget = R.defaultTo(
    widgetTypes.default,
    R.prop(R.prop('ui:widget', ui), widgetTypes)
  )
  return (
    <Widget
      ui={schema.ui}
      schema={schema}
      value={R.defaultTo(defaultValue, value)}
      onChange={onChange}
      type='number'
      required={schema.required}
      readOnly={schema.readOnly}
      error={error}
    />
  )

}

export default NumberField
