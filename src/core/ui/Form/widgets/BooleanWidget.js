import React from 'react'
import { Box, Switch } from '@material-ui/core'
import FormErrorWidget from './FormErrorWidget'

function BooleanWidget (props) {
  const { ui, value, onChange, error, required, readOnly, ...extraProps } = props
  return (
    <Box pt='10px'>
      <Switch
        checked={value}
        defaultValue={false}
        onChange={(e) => onChange(e.target.checked)}
        color='primary'
        required={required}
        readOnly={readOnly}
        {...extraProps}
        error={error}
      />
      <FormErrorWidget errors={error} />
    </Box>
  )
}

export default BooleanWidget
