import React, { useState, useEffect } from 'react'
import { Box, Button, Input } from '@material-ui/core'
import ReactCrop from 'react-image-crop'
import 'react-image-crop/dist/ReactCrop.css'
import { useHttp } from 'core/http'
import { useSnackbar } from 'notistack'

function ImageUploadWidget (props) {
  const {
    value,
    onChange
  } = props

  const { enqueueSnackbar } = useSnackbar()
  const http = useHttp()

  const [src, setSrc] = useState(props.src ? props.src : null)
  const [crop, setCrop] = useState({
    unit: '%',
    width: 30,
    aspect: 16 / 3
  })
  const [imageRef, setImageRef] = useState()
  const [file, setFile] = useState()
  const [croppedImage, setCroppedImage] = useState()
  const [croppedImageUrl, setCroppedImageUrl] = useState(value)
  const [fileUrl, setFileUrl] = useState()

  async function getUploadURL (file) {
    try {
      const res = await http.post(
        '/media/request-upload-url',
        { contentType: file.type, fileName: file.name }
      )
      return res
    } catch (e) {
      return e.response
    }
  }

  async function uploadByFile (file) {
    const getUploadURLRes = await getUploadURL(file)

    const {
      mediaPath,
      uploadUrl,
      url
    } = getUploadURLRes.data
    try {
      await http.put(uploadUrl, file, { headers: { 'Content-Type': file.type } })
      enqueueSnackbar('Sikeres képfeltöltés', { variant: 'success' })
      setCroppedImageUrl(url)
      onChange(mediaPath)
    } catch (e) {
      console.log(e)
      enqueueSnackbar('Sikertelen képfeltöltés', { variant: 'error' })
    }
  }

  useEffect(() => {
    if (croppedImage) {
      uploadByFile(croppedImage)
    }
  }, [croppedImage])

  function onSelectFile (e) {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader()
      reader.addEventListener('load', () =>
        setSrc(reader.result)
      )
      const file = e.target.files[0]
      reader.readAsDataURL(file)
      setFile(file)
    }
  }

  // If you setState the crop in here you should return false.
  function onImageLoaded (image) {
    setImageRef(image)
  }

  function onCropComplete (crop) {
    makeClientCrop(crop)
  }

  function onCropChange (crop, percentCrop) {
    // You could also use percentCrop:
    // this.setState({ crop: percentCrop });
    setCrop(crop)
  }

  async function makeClientCrop (crop) {
    if (imageRef && crop.width && crop.height) {
      const fileName = file.name
      const croppedImage = await getCroppedImg(
        imageRef,
        crop,
        file.name
      )
      setCroppedImage(croppedImage)
    }
  }

  function getCroppedImg (image, crop, fileName) {
    const canvas = document.createElement('canvas')
    const scaleX = image.naturalWidth / image.width
    const scaleY = image.naturalHeight / image.height
    canvas.width = crop.width
    canvas.height = crop.height
    const ctx = canvas.getContext('2d')

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    )

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          console.error('Canvas is empty')
          return
        }
        blob.name = file.name
        blob.lastModifiedDate = new Date()
        setCroppedImage(blob)
      }, file.type)
    })
  }

  return (
    <Box>
      <Box py={2}>
        {src && (
          <ReactCrop
            src={src}
            crop={crop}
            ruleOfThirds
            onImageLoaded={onImageLoaded}
            onComplete={onCropComplete}
            onChange={onCropChange}
          />
        )}
        {croppedImage ? null : croppedImageUrl && (
          <Box><img alt='Crop' style={{ maxWidth: '100%' }} src={croppedImageUrl} /></Box>
        )}
      </Box>
      <Button variant='contained' color='primary' component='label' size='small'>
        Fájl
        <Input type='file' accept='image/*' onChange={onSelectFile} style={{ display: 'none' }} />
      </Button>
    </Box>
  )
}

export default ImageUploadWidget
