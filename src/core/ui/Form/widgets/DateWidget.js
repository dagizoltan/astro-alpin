import 'date-fns'
import * as R from 'ramda'
import React, { useState } from 'react'
import DateFnsUtils from '@date-io/date-fns'
import {
  MuiPickersUtilsProvider,
  DatePicker
} from '@material-ui/pickers'
import FormErrorWidget from './FormErrorWidget'

function DateWidget (props) {
  const { ui, value, onChange, error } = props
  const [isOpen, setIsOpen] = useState(false)

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <DatePicker
        label={R.prop('ui:label', ui)}
        disabled={R.prop('ui:readonly', ui)}
        format='yyyy-MM-dd'
        value={value ? value : null}
        open={isOpen}
        onOpen={() => setIsOpen(true)}
        onChange={date => {
          onChange(date.toISOString().substring(0, 10))
          setIsOpen(false)
        }}
        error={error ? true : false}
        variant='inline'
      />
      <FormErrorWidget errors={error} />
    </MuiPickersUtilsProvider>
  )
}

export default DateWidget
