import React, { useMemo, useState } from 'react'
import * as R from 'ramda'
import { Chip, TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useQuery } from 'core/http'
import FormErrorWidget from './FormErrorWidget'

function MultipleAutocompleteWidget (props) {
  const { ui, value, onChange, error, required, readOnly, relatedModel } = props

  const [open, setOpen] = useState(false)
  const [keyword, setKeyword] = useState('')

  const { data, loading } = useQuery(relatedModel + '/?search=' + keyword)

  const options = useMemo(() => {
    const valueIds = value.map((x) => x._id)
    return R.pipe(
      R.propOr([], 'docs'),
      R.filter(R.complement(R.contains(R.__, valueIds))),
      R.map(R.pick(['_id', 'name']))
    )(data)
  }, [data])

  return (
    <>
      <Autocomplete
        multiple
        disabled={R.prop('ui:readonly', ui)}
        open={open}
        onOpen={() => setOpen(true)}
        onClose={() => { setOpen(false); setKeyword('') }}
        getOptionLabel={(option) => R.propOr('', 'name', option)}
        renderTags={(value, getTagProps) => value.map((option, index) =>
          <Chip key={index} size='small' label={option.name} {...getTagProps({ index })} />
        )}
        value={value}
        onChange={(e, v) => { onChange(v); setKeyword('') }}
        options={options}
        loading={loading}
        required={required}
        readOnly={readOnly}
        renderInput={(params) => {
          return (
            <TextField
              {...params}
              label={R.prop('ui:label', ui)}
              value={keyword}
              onChange={(e) => setKeyword(e.target.value)}
              fullWidth
              error={!!error}
              required={required}
              readOnly={readOnly}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <>
                    {loading ? <CircularProgress color='inherit' size={20} /> : null}
                    {params.InputProps.endAdornment}
                  </>
                )
              }}
            />
          )
        }}
      />
      <FormErrorWidget errors={error} />
    </>
  )
}

export default MultipleAutocompleteWidget
