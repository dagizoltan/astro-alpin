import React from 'react'
import * as R from 'ramda'
import { TextField } from '@material-ui/core'
import FormErrorWidget from './FormErrorWidget'

function DateTimeWidget (props) {
  const { ui, value, onChange, error } = props

  return (
    <>
      <TextField
        label={R.prop('ui:label', ui)}
        type='datetime-local'
        disabled={R.prop('ui:readonly', ui)}
        value={value || null}
        error={error}
        onChange={(e) => onChange(e.target.value)}
        fullWidth
        InputLabelProps={{
          shrink: true
        }}
      />
      <FormErrorWidget errors={error} />
    </>
  )
}

export default DateTimeWidget
