import React from 'react'
import MenuItem from '@material-ui/core/MenuItem'
import { Box, Select } from '@material-ui/core'
import FormErrorWidget from './FormErrorWidget'

function SelectWidget (props) {
  const { ui, onChange, error, value } = props
  return (
    <Box pt='15px'>
      <Select
        value={value}
        onChange={(e) => onChange(e.target.value)}
        error={error}
      >
        {ui['ui:choices'].map((choice, i) => <MenuItem key={i} value={choice.value}>{choice.displayName}</MenuItem>)}
      </Select>
      <FormErrorWidget errors={error} />
    </Box>
  )
}

export default SelectWidget
