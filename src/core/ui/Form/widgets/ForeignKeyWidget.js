import React from 'react'
import * as R from 'ramda'
import { AutocompleteWidget, MultipleAutocompleteWidget } from './'

function ForeignKeyWidget (props) {
  const { schema, ui, value, onChange, error, multiple } = props
  if (multiple) {
    return (<>
      <MultipleAutocompleteWidget
        ui={ui}
        value={R.defaultTo([], value)}
        onChange={onChange}
        relatedModel={R.prop('ui:relatedModel', schema.items.ui)}
        required={R.prop('ui:required', ui)}
        readOnly={R.prop('ui:readOnly', ui)}
        error={error}
      />
    </>)
  }
  return (
    <>
      <AutocompleteWidget
        ui={ui}
        value={R.defaultTo(null, value)}
        onChange={onChange}
        relatedModel={R.prop('ui:relatedModel', ui)}
        required={R.prop('ui:required', ui)}
        readOnly={R.prop('ui:readOnly', ui)}
        error={error}
      />
    </>
  )
}

export default ForeignKeyWidget