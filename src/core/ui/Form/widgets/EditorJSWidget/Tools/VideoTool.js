import React, { useState, useEffect } from 'react'
import { Box, Button, Typography, Input } from '@material-ui/core'
import { VideoPlayer } from 'core/ui/index'
import * as R from 'ramda'

function VideoTool (props) {
  const {
    onChange,
    config: {
      getSources,
      uploadByFile,
      type
    }
  } = props

  const [data, setData] = useState(R.defaultTo({ type: type }, props.data))
  // const { enqueueSnackbar } = useSnackbar()
  const [sources, setSources] = useState([])

  useEffect(() => {
    async function init (data) {
      if (data.file) {
        const sources = await getSources(data)
        console.log(sources)
        setSources(sources)
      }
    }
    console.log(data)

    init(data)
  }, [data])

  async function handleChange (e) {
    const file = e.target.files[0]

    const data = await uploadByFile(file)
    delete data.success
    setData(data)
    onChange(data)
  }

  console.log(sources)

  return (
    <Box py={3}>
      {sources.length > 0 ? <VideoPlayer sources={sources}/> : null}
      <Box mt={3}>
        <Button variant='contained' color='primary' component="label" size='small'>
          Fájl
          <Input id='mediaPath' type="file" accept="video/*" onChange={handleChange} style={{ display: "none" }} />
        </Button>
      </Box>
    </Box>
  )
}

export default VideoTool
