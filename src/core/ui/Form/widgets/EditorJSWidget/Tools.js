//import Raw from '@editorjs/raw'
import Header from '@editorjs/header'
//import Code from '@editorjs/code'
//import LinkTool from '@editorjs/link'
import ImageTool from '@editorjs/image'
import List from '@editorjs/list'
import Paragraph from '@editorjs/paragraph'
//import Embed from '@editorjs/embed'
import Table from '@editorjs/table'
import Warning from '@editorjs/warning'
import { useHttp } from 'core/http'
import { useSnackbar } from 'notistack'
import { useMemo } from 'react'
//import Quote from '@editorjs/quote'
//import Marker from '@editorjs/marker'
//import CheckList from '@editorjs/checklist'
//import Delimiter from '@editorjs/delimiter'
//import InlineCode from '@editorjs/inline-code'
import Tool from './Tools/Tool'


function useEditorJSTools (props) {
  const http = useHttp()
  const { enqueueSnackbar } = useSnackbar()

  return useMemo(() => {
    async function getUploadURL (file) {
      try {
        const res = await http.post(
          '/media/request-upload-url/',
          { contentType: file.type, filename: file.name }
        )
        return res
      } catch (e) {
        return e.response
      }
    }
    async function uploadByFile (file) {
      const getUploadURLRes = await getUploadURL(file)

      const {
        mediaPath,
        uploadUrl,
        url
      } = getUploadURLRes.data

      try {
        await http.put(uploadUrl, file, { headers: { 'Content-Type': file.type } })
        enqueueSnackbar('Sikeres képfeltöltés', { variant: 'success' })
        return {
          success: 1,
          file: {
            url: url,
            mediaPath: '/media/' + mediaPath
          }
        }
      } catch (e) {
        enqueueSnackbar('Sikertelen képfeltöltés', { variant: 'error' })
        return e.response
      }
    }

    async function getSources (data) {
      const {
        file: {
          mediaPath,
          url
        }
      } = data

      const path = mediaPath.substring(1)

      try {
        const res = await http.get('/media/get-video-metadata?path=' + mediaPath)
        console.log(res.data)
        return res.data
      } catch (e) {
        if (e.response.status === 404) {
          return [{ src: url }]
        }
        console.log(e)
        return e.response
      }
    }

    return {
      header: Header,
      paragraph: Paragraph,
      video: {
        class: Tool,
        config: {
          getSources: getSources,
          type: 'video',
          uploadByFile: uploadByFile
        }
      },
      imageTool: {
        class: ImageTool,
        config: {
          uploader: {
            uploadByFile: uploadByFile
          }
        }
      },
      table: {
        class: Table,
        inlineToolbar: true
      },
      list: List,
      warning: Warning

      /*
        embed: Embed,
        code: Code,
        linkTool: LinkTool,
        raw: Raw,
        quote: Quote,
        marker: Marker,
        checklist: CheckList,
        delimiter: Delimiter,
        inlineCode: InlineCode
      */
    }
  }, [http, enqueueSnackbar])
}

export default useEditorJSTools
