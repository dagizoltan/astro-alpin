import React from 'react'
import { Box } from '@material-ui/core'
import Editor from '@stfy/react-editor.js'
import useEditorJSTools from './Tools'
import * as R from 'ramda'

function EditorJSWidget (props) {
  const Tools = useEditorJSTools()
  const {
    value, onChange
  } = props
  return (
    <Box border={1}>
      <Editor
        tools={Tools}
        onData={(data) => { onChange(data) }}
        data={R.defaultTo({ blocks: [] }, value)}
      />
    </Box>
  )
}

export default EditorJSWidget
