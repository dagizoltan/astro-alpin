import React from 'react'
import * as R from 'ramda'
import MenuItem from '@material-ui/core/MenuItem'
import { Box, Select } from '@material-ui/core'
import FormErrorWidget from './FormErrorWidget'
import Chip from '@material-ui/core/Chip'

function MultiSelectWidget (props) {
  const { ui, onChange, error, value } = props

  return (
    <Box pt='15px'>
      <Select
        multiple
        fullWidth
        value={value || []}
        onChange={(e) => onChange(e.target.value)}
        error={error}
        renderValue={selected => (
          <div>
            {selected.map(value => (
              <Chip key={value} label={value} />
            ))}
          </div>
        )}
      >
        {ui['ui:choices'].map((choice) =>
          <MenuItem key={choice.displayName} value={choice.value} fontWeight={R.contains(choice.value, value) ? 'normal' : 'bold'}>
            {choice.displayName} ({choice.value})
          </MenuItem>
        )}
      </Select>
      <FormErrorWidget errors={error} />
    </Box>
  )
}

export default MultiSelectWidget
