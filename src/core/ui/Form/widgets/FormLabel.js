import React from 'react'
import { Box, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

function FormLabel (props) {
  const classes = useStyles()

  const {
    label,
    required
  } = props

  return <Box my={1}><Typography className={classes.label}>{label}{required ? ' *' : null}</Typography></Box>
}

const useStyles = makeStyles(theme => ({
  label: {
    width: 200,
    fontWeight: '500',
    textAlign: 'right',
    paddingRight: 40,
    color: theme.palette.primary.dark,
    textTransform: 'capitalize'
  }
}))

export default FormLabel
