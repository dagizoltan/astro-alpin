import React, { useMemo, useState } from 'react'
import * as R from 'ramda'
import { TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useQuery } from 'core/http'
import FormErrorWidget from './FormErrorWidget'

function AutocompleteWidget (props) {
  const { ui, value, onChange, error, required, readOnly } = props

  const [open, setOpen] = useState(false)
  const [keyword, setKeyword] = useState('')

  const relatedModel = ui['ui:relatedModel']

  const { data, loading } = useQuery(relatedModel + '/?search=' + keyword)

  const options = useMemo(() =>
    R.pipe(
      R.propOr([], 'docs'),
      R.map(R.pick(['_id', 'name']))
    )(data)
  , [data])

  return (
    <>
      <Autocomplete
        disabled={R.prop('ui:readonly', ui)}
        open={open}
        onOpen={() => { setOpen(true) }}
        onClose={() => { setOpen(false); setKeyword('') }}
        getOptionSelected={(option, value) => option.id === value.id}
        getOptionLabel={(option) => R.propOr('', 'name', option)}
        value={value}
        onChange={(e, v) => onChange(v)}
        options={options}
        loading={loading}
        required={required}
        readOnly={readOnly}
        renderInput={(params) => {
          return (
            <TextField
              {...params}
              label={R.prop('ui:label', ui)}
              value={keyword}
              onChange={(e) => setKeyword(e.target.value)}
              fullWidth
              error={!!error}
              required={required}
              readOnly={readOnly}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <>
                    {loading ? <CircularProgress color='inherit' size={20} /> : null}
                    {params.InputProps.endAdornment}
                  </>
                )
              }}
            />
          )
        }}
      />
      <FormErrorWidget errors={error} />
    </>
  )
}

export default AutocompleteWidget
