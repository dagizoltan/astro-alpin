import React from 'react'
import { Typography } from '@material-ui/core'

function FormErrorWidget (props) {
  const { errors } = props

  if (errors) return errors.map((error, i) => <Typography key={i} variant='caption' color='error'>{error}</Typography>)
  return null
}

export default FormErrorWidget
