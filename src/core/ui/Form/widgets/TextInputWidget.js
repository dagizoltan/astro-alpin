import React from 'react'
import * as R from 'ramda'
import { TextField } from '@material-ui/core'
import FormErrorWidget from './FormErrorWidget'

function TextInputWidget (props) {
  const { ui, value, onChange, error, ...extraProps } = props
  return (
    <>
      <TextField
        label={R.prop('ui:label', ui)}
        placeholder={R.prop('ui:placeHolder', ui)}
        type={R.prop('ui:widget', ui) ? R.prop('ui:widget', ui) : 'text'}
        disabled={R.prop('ui:readonly', ui)}
        value={value}
        onChange={(e) => onChange(e.target.value)}
        fullWidth
        error={!!error}
        {...extraProps}
      />
      <FormErrorWidget errors={error} />
    </>
  )
}

export default TextInputWidget
