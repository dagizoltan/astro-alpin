import React, { useState } from 'react'
import Form from './Form'

const groupsSchema = {
  type: 'object',
  properties: {
    name: {
      title: 'Név',
      type: 'string',
      required: true
    },
    rules: {
      title: 'Szabályok',
      type: 'array',
      required: false,
      readOnly: false,
      items: {
        type: 'object',
        properties: {
          prohibitive: {
            ui: {},
            actionType: 'boolean',
            choices: false,
            required: false,
            readOnly: false,
            title: 'Tiltó szabály',
            type: 'boolean',
            default: false
          },
          chain: {
            title: 'Struktúra',
            type: 'object',
            properties: {
              baseUser: {
                ui: {
                  'ui:widget': 'foreignkey',
                  'ui:relatedModel': 'users/users'
                },
                actionType: 'field',
                choices: false,
                required: false,
                readOnly: false,
                title: 'Referencia felhasználó',
                type: 'number'
              },
              minimumLeaderLevel: {
                ui: {
                  'ui:widget': 'foreignkey',
                  'ui:relatedModel': 'users/userlevels'
                },
                actionType: 'field',
                choices: false,
                required: true,
                readOnly: false,
                title: 'Minimum vezetői szint',
                type: 'string'
              },
              chainruleType: {
                ui: {},
                actionType: 'choice',
                choices: [
                  {
                    value: 'R',
                    displayName: 'Beszervezői lánc'
                  },
                  {
                    value: 'L',
                    displayName: 'Vezetői lánc'
                  },
                  {
                    value: 'P',
                    displayName: 'Párhuzamos vezetői lánc'
                  }
                ],
                required: false,
                readOnly: false,
                title: 'Láncszabály típusa',
                type: 'string',
                enum: [
                  'R',
                  'L',
                  'P'
                ],
                enumNames: [
                  'Beszervezői lánc',
                  'Vezetői lánc',
                  'Párhuzamos vezetői lánc'
                ],
                default: 'R'
              },
              direction: {
                ui: {},
                actionType: 'choice',
                choices: [
                  {
                    value: 'U',
                    displayName: 'Felfele'
                  },
                  {
                    value: 'D',
                    displayName: 'Lefele'
                  }
                ],
                required: false,
                readOnly: false,
                title: 'Irány',
                type: 'string',
                enum: [
                  'U',
                  'D'
                ],
                enumNames: [
                  'Felfele',
                  'Lefele'
                ],
                default: 'D'
              },
              distance: {
                ui: {},
                actionType: 'integer',
                choices: false,
                required: false,
                readOnly: false,
                title: 'Távolság',
                type: 'number',
                default: 20
              }
            }
          },
          office: {
            title: 'Iroda',
            type: 'object',
            properties: {
              offices: {
                ui: {},
                actionType: 'field',
                choices: false,
                required: true,
                readOnly: false,
                title: 'Irodák',
                type: 'array',
                items: {
                  type: 'number',
                  ui: {
                    'ui:widget': 'foreignkey',
                    'ui:relatedModel': 'users/offices'
                  }
                }
              }
            }
          },
          rank: {
            title: 'Rang',
            type: 'object',
            properties: {
              levels: {
                ui: {},
                actionType: 'multiple choice',
                choices: [
                  {
                    value: 'KP',
                    displayName: 'Központi munkatárs - KP'
                  },
                  {
                    value: 'KP2',
                    displayName: 'Központi munkatárs - KP2'
                  },
                  {
                    value: 'LD',
                    displayName: 'Országos igazgató - LD'
                  },
                  {
                    value: 'RD',
                    displayName: 'Régióigazgató - RD'
                  },
                  {
                    value: 'BD',
                    displayName: 'Területi igazgató - BD'
                  },
                  {
                    value: 'BL',
                    displayName: 'Területi vezető - BL'
                  },
                  {
                    value: 'GS',
                    displayName: 'Fiókvezető - GS'
                  },
                  {
                    value: 'GS+',
                    displayName: 'Fiókvezető - GS+'
                  },
                  {
                    value: 'VR',
                    displayName: 'Vezető reprezentáns - VR'
                  },
                  {
                    value: 'VR2',
                    displayName: 'Fiókvezető mérő - VR2'
                  },
                  {
                    value: 'R5',
                    displayName: 'R5 reprezentáns'
                  },
                  {
                    value: 'R4',
                    displayName: 'R4 reprezentáns'
                  },
                  {
                    value: 'R3',
                    displayName: 'R3 reprezentáns'
                  },
                  {
                    value: 'R2',
                    displayName: 'R2 reprezentáns'
                  },
                  {
                    value: 'R1',
                    displayName: 'R1 reprezentáns'
                  },
                  {
                    value: 'PA',
                    displayName: 'Partner - PA'
                  }
                ],
                required: true,
                readOnly: false,
                title: 'Felhasználó szint (rang)',
                type: 'string',
                default: {}
              }
            }
          },
          user: {
            title: 'Felhasználó',
            type: 'object',
            properties: {
              users: {
                ui: {},
                actionType: 'field',
                choices: false,
                required: true,
                readOnly: false,
                title: 'Felhasználók',
                type: 'array',
                items: {
                  type: 'number',
                  ui: {
                    'ui:widget': 'foreignkey',
                    'ui:relatedModel': 'users/users'
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

export default { title: 'core.ui.forms.Form' }

export function groupForm () {
  function GroupForm (props) {
    const [formData, setFormData] = useState(null)

    return (
      <Form
        schema={groupsSchema}
        value={formData}
        onChange={setFormData}
        error={null}
      />
    )
  }

  return <GroupForm />
}
