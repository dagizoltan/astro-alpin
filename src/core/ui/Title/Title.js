import React, { useEffect } from 'react'
import { Typography, Grid } from '@material-ui/core'
import useStyles from './useStyles'

function Title (props) {
  const {
    label
  } = props

  useEffect(() => {
  })

  const classes = useStyles()

  return (
    <Grid container direction='column' justify='center' alignItems='center'>

      <Typography data-aos='fade-down' variant='h1' className={classes.title}>{label}</Typography>
      {/* <Divider variant='middle' className={classes.divider} /> */}
    </Grid>
  )
}

export default Title
