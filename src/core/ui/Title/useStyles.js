import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({

  divider: {
    height: '6px',
    borderRadius: '3px',
    backgroundColor: theme.palette.secondary.light,
    minWidth: '50%'
  },
  title: {
    fontFamily: theme.fontfamily.title,
    marginBottom: theme.spacing(3),
    textAlign: 'center',
    color: theme.palette.secondary.light
  }

}))

export default useStyles
