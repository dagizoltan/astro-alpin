import React from 'react'
import { TableCell } from '@material-ui/core'

function DataCells (props) {
  const {
    row
  } = props

  return (
    <>
      {row.cells.map((cell, j) =>
        <TableCell key={j} {...cell.getCellProps()}>
          {cell.render('Cell')}
        </TableCell>
      )}
    </>
  )
}

export default DataCells
