import React from 'react'

import {
  TableCell, Icon
} from '@material-ui/core'

function HeaderCell (props) {
  const {
    column
  } = props

  return (
    <TableCell {...column.getHeaderProps(column.getSortByToggleProps())}>
      {column.render('Header')}
      <span>
        {column.isSorted
          ? column.isSortedDesc
            ? <Icon style={{ verticalAlign: 'middle' }}>keyboard_arrow_down</Icon>
            : <Icon style={{ verticalAlign: 'middle' }}>keyboard_arrow_up</Icon>
          : ''}
      </span>
    </TableCell>
  )
}

export default HeaderCell
