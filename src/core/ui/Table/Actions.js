import React from 'react'
import { IconButton, ButtonGroup } from '@material-ui/core'
function Actions (props) {
  const {
    row,
    actions
  } = props

  return (
    <ButtonGroup size='small'>
      {actions.map((x, i) =>
        <IconButton
          key={i}
          variant='text'
          onClick={() => x.handler(row.values)}
        >
          <x.icon
            fontSize='small'
          />
        </IconButton>
      )}
    </ButtonGroup>
  )
}

export default Actions
