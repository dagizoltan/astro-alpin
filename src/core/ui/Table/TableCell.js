import React, { useMemo } from 'react'
import * as R from 'ramda'
import moment from 'moment'
import { Chip } from '@material-ui/core'
import { Check as CheckIcon, Clear as ClearIcon } from '@material-ui/icons'

function TableCell (props) {
  const { cell: { value } } = props

  const normalizedValue = useMemo(() => {
    if (R.is(String, value) && value.length === 27) {
      // possible date
      const dateValue = moment(value)
      if (dateValue.isValid()) {
        return dateValue
      }
    }
    return value
  }, [value])

  if (R.is(Array, normalizedValue)) {
    return normalizedValue.map((x, k) =>
      <Chip key={k} label={x.name} size='small' />
    )
  }

  if (R.is(Boolean, normalizedValue)) {
    return normalizedValue
      ? <CheckIcon />
      : <ClearIcon />
  }

  if (R.is(Object, normalizedValue) && normalizedValue.name) {
    return normalizedValue.name
  }

  if (moment.isMoment(normalizedValue)) {
    return normalizedValue.format('lll')
  }

  return String(normalizedValue)
}

export default TableCell
