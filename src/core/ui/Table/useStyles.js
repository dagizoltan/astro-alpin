import { makeStyles } from '@material-ui/core/styles'
const useStyles = makeStyles(theme => ({

  paper: {
    width: '100%',
    marginBottom: theme.spacing(2)
  },
  table: {
    minWidth: 750,
    head: {
      backgroundColor: theme.palette.primary.main
    }
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1
  },
  paginator: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10
  },
  paginatorSelector: {
    width: 150,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    marginRight: 20
  },
  page: {
    fontSize: 15
  },
  row: {
    cursor: ({ onRowPress }) => onRowPress ? 'pointer' : 'inherit',
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover
    }
  },
  toggleButton: {
    borderRadius: '50%'
  }
}))

export default useStyles
