import React from 'react'
import { useTable, useSortBy, usePagination } from 'react-table'
import * as R from 'ramda'
import {
  Box, Table as T,
  TableContainer,
  TableHead, TableBody, TableRow, TableCell
} from '@material-ui/core'
import { Pagination, Alert, ToggleButton, ToggleButtonGroup } from '@material-ui/lab'

import { LoadingIndicator } from 'core/ui'
import { useLocale } from 'locale'
import useStyles from './useStyles'

import HeaderCell from './HeaderCell'
import DataCells from './DataCells'
import Actions from './Actions'

function Table (props) {
  const {
    columns, data, rowCount,
    orderField, setOrderField,
    orderDirection, setOrderDirection,
    page, setPage,
    pageSize, setPageSize,
    onRowPress, actions = [], loading
  } = props

  const classes = useStyles({ onRowPress })

  const { _ } = useLocale()

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    rows
  } = useTable(
    {
      columns,
      data,
      pageCount: Math.ceil(rowCount / pageSize),
      manualPagination: true,
      manualSortBy: true,
      initialState: {
        pageIndex: page,
        pageSize: pageSize,
        sortBy: [{ id: orderField, desc: orderDirection === 'desc' }]
      },
      useControlledState: (state) => {
        setOrderField(R.path(['sortBy', 0, 'id'], state))
        setOrderDirection(R.path(['sortBy', 0, 'desc'], state) ? 'desc' : 'asc')
        return {
          ...state,
          pageIndex: page
        }
      }
    },
    useSortBy,
    usePagination
  )

  return (
    <LoadingIndicator loading={loading}>
      {rows.length === 0
        ? (
          <Box mb={1}>
            <Alert severity='warning' variant='filled'>A tábla üres vagy nincs megfelelő találat a szűrési feltételekre</Alert>
          </Box>
        ) : null
      }
      <TableContainer>
        <T {...getTableProps()} size='small' fullWidth className={classes.table}>
          <TableHead>
            {headerGroups.map((headerGroup, i) => (
              <TableRow key={i} {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => <HeaderCell key={column.id} column={column} />)}
                <TableCell align='right' />
              </TableRow>
            ))}
          </TableHead>
          <TableBody {...getTableBodyProps()}>
            {rows.map(
              (row, i) => {
                prepareRow(row)

                return (
                  <TableRow key={i} {...row.getRowProps()} onClick={onRowPress ? () => onRowPress(row.values) : null} className={classes.row}>
                    <DataCells row={row} />
                    <TableCell align='right'>
                      <Actions actions={actions} row={row} />
                    </TableCell>
                  </TableRow>
                )
              }
            )}
          </TableBody>
        </T>
        <Box className={classes.paginator}>
          <Box />
          <Pagination
            size='small'
            count={Math.floor(rowCount / pageSize) + 1}
            page={page}
            onChange={(e, v) => setPage(v)}
            boundaryCount={2}
          />
          <ToggleButtonGroup size='small' value={pageSize} exclusive onChange={(e, x) => setPageSize(x)}>
            {[10, 25, 50].map(x => <ToggleButton key={x} value={x} className={classes.toggleButton}>{x}</ToggleButton>)}
          </ToggleButtonGroup>
        </Box>
      </TableContainer>
    </LoadingIndicator>
  )
}

export default Table
