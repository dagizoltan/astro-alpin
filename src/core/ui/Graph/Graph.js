import React from 'react'
import { LineChart, XAxis, Tooltip, CartesianGrid, Line, Legend, ResponsiveContainer } from 'recharts'
import { Box } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { useQuery } from 'core/websocket'
import { LoadingIndicator } from 'core/ui'

function Graph (props) {
  const { data, initialized } = useQuery(props)

  if (!initialized) return <LoadingIndicator />

  if (data[0]) {
    const keys = Object.keys(data[0].data)
    return (
      <Box height='400px'>
        {data.length}
        <ResponsiveContainer>
          <LineChart
            data={data}
          >
            <XAxis dataKey='createdAt' padding={{ left: 30, right: 30 }} />
            <Tooltip />
            <CartesianGrid stroke='#f5f5f5' />
            <Legend />
            {keys.map((key) => {
              return <Line key={key} type='monotone' dataKey={`data.${key}`} stroke='#ff0000' yAxisId={0} activeDot={{ r: 0 }} />
            })}
          </LineChart>
        </ResponsiveContainer>
      </Box>
    )
  } else {
    return <Alert severity='info'>Measurement is not started yet</Alert>
  }
}

export default Graph
