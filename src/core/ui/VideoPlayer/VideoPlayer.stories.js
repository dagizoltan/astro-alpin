import React from 'react'
import VideoPlayer from '.'

export default { title: 'ui.VideoPlayer' }

export const normal = () =>
  <VideoPlayer sources={[
    {
      src: 'http://media.xiph.org/mango/tears_of_steel_1080p.webm',
      type: 'video/webm',
      label: 'SD',
      res: '360'
    },
    {
      src: 'http://mirrorblender.top-ix.org/movies/sintel-1024-surround.mp4',
      type: 'video/mp4',
      label: 'HD',
    }
  ]}
  />
