import React, { useEffect, useRef, useState } from 'react'
import {
  Box
} from '@material-ui/core'

import videojs from 'video.js'

import 'videojs-resolution-switcher/lib/videojs-resolution-switcher.css'
import 'video.js/dist/video-js.css'

const usePlayer = (props) => {
  const options = {
    controls: true,
    fill: true,
    fluid: true,
    preload: 'auto',
    html5: {
      hls: {
        enableLowInitialPlaylist: true,
        smoothQualityChange: true,
        overrideNative: true
      }
    }
    /*
    plugins: {
      videoJsResolutionSwitcher: {
        default: 'SD'
      }
    }
    */
  }

  const videoRef = useRef(null)
  const [player, setPlayer] = useState(null)

  useEffect(() => {
    window.videojs = videojs
    const vjsPlayer = videojs(videoRef.current, {
      ...options,
      ...props
    })
    vjsPlayer.src(props.sources[0])
    setPlayer(vjsPlayer)

    return () => {
      if (player !== null) {
        player.dispose()
      }
    }
  }, [])

  return videoRef
}

function VideoPlayer (props) {
  const playerRef = usePlayer(props)

  return (
    <Box>
      <video ref={playerRef} className='video-js' />
    </Box>
  )
}

export default VideoPlayer
