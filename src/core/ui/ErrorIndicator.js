import React from 'react'
import { Box, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { Alert } from '@material-ui/lab'

function ErrorIndicator (props) {
  const { error, retry } = props

  const classes = useStyles()

  const action = retry
    ? (
      <Button color='inherit' size='small' onClick={retry}>
        Retry
      </Button>
    ) : undefined

  return (
    <Box className={classes.container}>
      <Box className={classes.box}>
        <Alert severity='error' variant='filled' action={action}>
          {error.message}
        </Alert>
      </Box>
    </Box>
  )
}

const useStyles = makeStyles(theme => ({
  container: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  box: {
    maxWidth: 600
  }
}))

export default ErrorIndicator
