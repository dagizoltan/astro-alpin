import React from 'react'
import { Box, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

function Card (props) {
  const {
    defaultExpanded,
    expanded,
    onExpandChange,
    icon,
    title,
    children
  } = props
  const classes = useStyles()

  return (
    <ExpansionPanel defaultExpanded={defaultExpanded} expanded={expanded} onChange={onExpandChange}>
      <ExpansionPanelSummary
        classes={{
          root: classes.summaryRoot,
          expandIcon: classes.summaryExpandIcon,
          content: classes.summaryContent
        }}
        expandIcon={<ExpandMoreIcon />}
      >
        <Box classes={{ root: classes.iconBox }}>{icon}</Box>
        <Typography>{title}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        {children}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  )
}

const useStyles = makeStyles((theme) => ({
  summaryRoot: {
    background: '#fff',
    borderBottom: '2px solid #00b7e5',
    color: '#fff',
    textTransform: 'uppercase',
    minHeight: '10px',
    '&.Mui-expanded': {
      minHeight: '10px'
    },
    paddingLeft: 0
  },
  summaryExpandIcon: {
    color: '#777'
  },
  summaryContent: {
    color: '#111',
    margin: '5px 0',
    '&.Mui-expanded': {
      margin: '5px 0'
    }
  },
  iconBox: {
    color: '#fff',
    background: theme.palette.info.main,
    marginTop: '-18px',
    marginBottom: '-10px',
    marginLeft: '-5px',
    marginRight: '10px',
    width: '50px',
    height: '50px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
}))

export default Card
