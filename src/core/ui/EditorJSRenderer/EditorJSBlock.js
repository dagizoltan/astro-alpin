import React from 'react'
import * as R from 'ramda'
import Blocks from './Blocks'

function EditorJSBlock (props) {
  const { type, light } = props
  const Block = R.defaultTo(
    Blocks.default,
    R.prop(type, Blocks)
  )

  return <Block {...props} light={light}/>
}

export default EditorJSBlock
