import HeaderBlock from './HeaderBlock'
import ImageBlock from './ImageBlock'
//import LinkTool from './LinkTool'
import ListBlock from './ListBlock'
import ParagraphBlock from './ParagraphBlock'
import InvalidBlockType from './InvalidBlockType'
import AlertBlock from './AlertBlock'
import TableBlock from './TableBlock'

const Blocks = {
  warning: AlertBlock,
  header: HeaderBlock,
  imageTool: ImageBlock,
  //linkTool: LinkTool,
  list: ListBlock,
  paragraph: ParagraphBlock,
  table: TableBlock,
  default: InvalidBlockType
}

export default Blocks
