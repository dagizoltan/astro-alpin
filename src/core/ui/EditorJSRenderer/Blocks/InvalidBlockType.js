import React from 'react'
import { Box } from '@material-ui/core'
import { Alert } from '@material-ui/lab'

function InvalidBlockType (props) {
  const { type } = props
  return <Box my={2}><Alert severity='error' variant='outlined'>Invalid Block type: "{type}"</Alert></Box>
}

export default InvalidBlockType
