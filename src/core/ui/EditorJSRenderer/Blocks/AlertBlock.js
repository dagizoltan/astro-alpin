import React from 'react'
import { Box } from '@material-ui/core'
import { Alert, AlertTitle } from '@material-ui/lab'

function AlertBlock (props) {
  const { data } = props
  return (
    <Box>
      <Alert severity='warning'>
        <AlertTitle>{data.title}</AlertTitle>
        {data.message}
      </Alert>
    </Box>
  )
}

export default AlertBlock
