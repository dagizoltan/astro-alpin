import React from 'react'
import { Box, Typography } from '@material-ui/core'

function ParagraphBlock (props) {
  const {
    data,
    light
  } = props

  return <Box color={light ? 'white' : ''}><Typography varinat='body1' align='justify'>{data.text}</Typography></Box>
}

export default ParagraphBlock
