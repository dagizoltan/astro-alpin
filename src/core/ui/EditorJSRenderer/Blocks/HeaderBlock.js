import React from 'react'
import { Box, Typography } from '@material-ui/core'

function HeaderBlock (props) {
  const {
    data
  } = props

  return (
    <Box mb={2}>
      <Typography variant={'h' + data.level} color='primary'>{data.text}</Typography>
    </Box>)
}

export default HeaderBlock
