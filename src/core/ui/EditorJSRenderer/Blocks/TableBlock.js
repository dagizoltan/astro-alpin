import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

function TableBlock (props) {
  const { data } = props
  const header = data.content[0]
  const body = data.content.slice(1)
    const classes = useStyles()

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            {header.map(cell => <TableCell component="th" scope="row">{cell}</TableCell>)}
          </TableRow>
        </TableHead>
        <TableBody>
        {body.map(row => <TableRow>{row.map(cell => <TableCell component="th" scope="row">{cell}</TableCell>)}</TableRow>)}
        </TableBody>
      </Table>
    </TableContainer>
  );  
}

const useStyles = makeStyles(theme => ({
  table: {
    minWidth: 650,
  },
}))

export default TableBlock
