import React from 'react'
import { List, ListItem } from '@material-ui/core'

function ListBlock (props) {
  const { data: { items } } = props

  return (
    <List>
      {items.map(item => <ListItem>{item}</ListItem>)}
    </List>
  )
}

export default ListBlock
