import React from 'react'
import { Box, CardMedia, Typography } from '@material-ui/core'
import { useQuery } from 'core/http'
import LoadingIndicator from 'core/ui/LoadingIndicator'

function ImageBlock (props) {
  const {
    data: {
      file: {
        mediaPath
      },
      caption
    }
  } = props

  const { data, initialized, error, refetch } = useQuery(mediaPath)

  if (!initialized) return <LoadingIndicator />
  const {
    url
  } = data

  return (
    <Box width={2/3} py={2} mx='auto'>
      <CardMedia
        component='img'
        alt={caption}
        image={url}
        title={caption}
      />
      <Typography variant='caption'>{caption}</Typography>
    </Box>
  )
}

export default ImageBlock
