import React from 'react'
import { Box } from '@material-ui/core'
import EditorJSBlock from './EditorJSBlock'

function EditorJSRenderer (props) {
  const { blocks, light } = props

  return (
    <Box py={2}>
      {
        blocks.map((block, i) =>
          <Box key={i} pt={i === 0 ? 0 : 1} pb={i === blocks.length - 1 ? 0 : 1}>
            <EditorJSBlock {...block} light={light} />
          </Box>
        )
      }
    </Box>
  )
}

export default EditorJSRenderer
