import { useContext, useMemo } from 'react'
import ListenerContext from './ListenerContext'

function useEventKey (key) {
  const { getEventKey, triggerEventKey } = useContext(ListenerContext)

  return [
    getEventKey(key),
    useMemo(() =>
      () => triggerEventKey(key)
    , [triggerEventKey, key])
  ]
}

export default useEventKey
