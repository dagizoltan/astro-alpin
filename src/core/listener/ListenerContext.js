import React, { useState, useMemo } from 'react'
import * as R from 'ramda'

const ListenerContext = React.createContext()

const incrementKey = (key) =>
  R.over(R.lensProp(key), R.pipe(R.defaultTo(0), R.inc))

export function ListenerProvider (props) {
  const { children } = props

  const [eventKeys, setEventKeys] = useState({})

  const getEventKey = useMemo(() => {
    return (key) => key + '-' + R.defaultTo(0, R.prop(key, eventKeys))
  }, [eventKeys])

  const triggerEventKey = useMemo(() => {
    return (key) => setEventKeys(incrementKey(key)(eventKeys))
  }, [eventKeys])

  const value = {
    getEventKey,
    triggerEventKey
  }

  return (
    <ListenerContext.Provider value={value}>
      {children}
    </ListenerContext.Provider>
  )
}

export default ListenerContext
