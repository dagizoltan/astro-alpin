import { useContext } from 'react'
import WebsocketContext from '../context/WebSocketContext'

function useWebsocket () {
  return useContext(WebsocketContext)
}

export default useWebsocket
