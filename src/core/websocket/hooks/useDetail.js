import { useState, useEffect } from 'react'
import useWebsocket from './useWebsocket'

function useDetail (props) {
  const { id, module } = props

  const detail = `${module}/detail`
  const socket = useWebsocket()
  const [data, setData] = useState()
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    socket.emit('subscribe', `${detail}/${id}`)
    return () => {
      socket.emit('unsubscribe', `${detail}/${id}`)
    }
  }, [])

  useEffect(() => {
    socket.emit(`req:detail:${module}`, { id })

    socket.on(`res:detail:${module}`, (payload) => {
      setData(payload)
      setLoading(false)
    })
    return () => {
      socket.off(`res:detail:${module}`)
    }
  }, [])

  useEffect(() => {
    socket.on(`res:patch:${module}`, (payload) => {
      setData({ ...data, ...payload })
    })
    return () => {
      socket.off(`res:patch:${module}`)
    }
  })

  return {
    loading,
    data
  }
}

export default useDetail
