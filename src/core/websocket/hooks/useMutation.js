import { useCallback, useState } from 'react'
import useWebsocket from './useWebsocket'
import { useNotification } from 'core/notification'

function useMutation (props) {
  const {
    method,
    module,
    config = {}
  } = props

  const notification = useNotification()
  const socket = useWebsocket()

  const initialState = {
    loading: false
  }

  const [state, setState] = useState(initialState)

  const mutate = useCallback(async (data) => {
    setState({
      loading: true
    })
    socket.emit(`req:${method}:${module}`, data)
  })

  const mutationListener = () => {
    socket.on(`res:${method}:${module}`, (payload) => {
      const {
        status
      } = payload

      notification.http(status)
      setState({
        loading: false
      })
      if (config.onSuccess) config.onSuccess()
    })
    return () => {
      socket.off(`res:${method}:${module}`)
    }
  }

  return [
    mutate,
    mutationListener,
    state
  ]
}

export default useMutation
