import { useState, useEffect } from 'react'
import useWebsocket from './useWebsocket'

function useQuery (props) {
  const { module, queryConfig } = props

  const [data, setData] = useState()
  const [loading, setLoading] = useState(true)

  const socket = useWebsocket()

  useEffect(() => {
    socket.emit(`req:${module}`, { queryConfig })

    socket.on(`res:${module}`, (data) => {
      setData(data)
      setLoading(false)
    })

    return () => {
      socket.off(`res:${module}`)
      setData()
    }
  }, [])

  return {
    initialized: !loading,
    data
  }
}

export default useQuery
