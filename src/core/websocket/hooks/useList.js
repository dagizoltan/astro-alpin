import { useState, useEffect } from 'react'
import useWebsocket from './useWebsocket'
import { useScreen } from 'core/screen'

function useList (props) {
  const { queryConfig, route } = props
  const socket = useWebsocket()

  const list = `list:${route}`

  const [data, setData] = useState()
  const [initialized, setInitialized] = useState(false)

  const screen = useScreen()
  const isActive = screen ? screen.isActive : false

  useEffect(() => {
    setInitialized(false)
    socket.emit(`req:${list}`, { queryConfig })

    socket.on(`res:${list}`, (data) => {
      setData(data)
      setInitialized(true)
    })

    return () => {
      socket.off(`res:${list}`)
      setData()
    }
  }, [queryConfig, isActive])

  function fetch (queryConfig) {
    socket.emit(`req:${list}`, { queryConfig })
  }

  return {
    initialized,
    data,
    fetch
  }
}

export default useList
