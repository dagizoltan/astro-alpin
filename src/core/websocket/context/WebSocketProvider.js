import React, { useState } from 'react'
import * as io from 'socket.io-client'

import WebsocketContext from './WebSocketContext'

export function WebsocketProvider (props) {
  const {
    children
  } = props

  const [connected, setConnected] = useState(false)

  const websocketInstance = io('http://localhost:3010/', {
    secure: true,
    rejectUnauthorized: false,
    path: '/sockets',
    transports: ['websocket']
  })

  return (
    <WebsocketContext.Provider value={websocketInstance}>
      {children}
    </WebsocketContext.Provider>
  )
}

export default WebsocketProvider
