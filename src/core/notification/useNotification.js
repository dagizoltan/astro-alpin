import { useMemo } from 'react'
import { useSnackbar } from 'notistack'
import { useLocale } from 'locale'

function useNotification () {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar()
  const { _ } = useLocale()

  const httpMessages = useMemo(() => {
    return {
      200: {
        message: _('Success'),
        variant: 'success'
      },
      201: {
        message: _('Created'),
        variant: 'success'
      },
      202: {
        message: _('Successully Modified'),
        variant: 'success'
      },
      204: {
        message: _('Successful delete'),
        variant: 'success'
      },
      400: {
        message: _('Validation error'),
        variant: 'error'
      },
      500: {
        message: _('Server error'),
        variant: 'error'
      }
    }
  }, [])

  return useMemo(() => {
    return {
      closeSnackbar: (key) => {
        closeSnackbar(key)
      },
      info: (message) => {
        enqueueSnackbar(message, { variant: 'info' })
      },
      http: (status) => {
        enqueueSnackbar(httpMessages[status].message, { variant: httpMessages[status].variant })
      },
      customNotification: (message, options) => {
        enqueueSnackbar(message, options)
      }
    }
  }, [enqueueSnackbar, httpMessages])
}

export default useNotification
