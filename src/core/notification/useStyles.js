import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  success: { backgroundColor: theme.palette.success.main },
  error: { backgroundColor: theme.palette.error.main },
  warning: { backgroundColor: theme.palette.warning.main },
  info: { backgroundColor: theme.palette.info.main }
})
)

export default useStyles
