import React from 'react'
import { Slide } from '@material-ui/core'
import { SnackbarProvider } from 'notistack'
import useStyles from './useStyles'

function SlideTransition (props) {
  return <Slide {...props} direction='left' />
}

function NotificationProvider (props) {
  const { children } = props
  const classes = useStyles()

  return (
    <SnackbarProvider
      maxSnack={5}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      autoHideDuration={2500}
      key='top,horizontal'
      TransitionComponent={SlideTransition}
      classes={{
        variantSuccess: classes.success,
        variantError: classes.error,
        variantWarning: classes.warning,
        variantInfo: classes.info
      }}
    >
      {children}
    </SnackbarProvider>
  )
}

export default NotificationProvider
