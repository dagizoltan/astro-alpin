
export default {
  apiBaseUrl: window.location.host.indexOf('localhost') === -1
    ? 'http://127.0.0.1:3010/api'
    : 'http://127.0.0.1:3010/api'
}
