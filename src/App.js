import React from 'react'
import moment from 'moment'

import { MainNavigationContainer } from './navigation/containers'

import ProviderGroup from './ProviderGroup'

require('moment/locale/hu')
moment.locale('hu')

function App () {
  return <MainNavigationContainer />
}

function AppProvider (props) {
  return (
    <ProviderGroup>
      <App />
    </ProviderGroup>
  )
}

export default AppProvider
