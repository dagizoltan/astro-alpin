import React from 'react'
import { Box } from '@material-ui/core'
import { useNavigation } from '../hooks'
import { Screen, MainLayout } from 'layout/components'

function MainNavigationContainer (props) {
  const { screens, error } = useNavigation()

  if (error) {
    return error
  }

  return (
    <MainLayout>
      {screens.length > 0
        ? (
          <Box display='flex' position='relative' height='100%'>
            {screens.map((x, i) =>
              <Screen key={i} stackIndex={i} active={i === screens.length - 1}>
                <x.component queryParams={x.params} />
              </Screen>
            )}
          </Box>
        ) : null}
    </MainLayout>
  )
}

export default MainNavigationContainer
