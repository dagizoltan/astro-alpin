import { useMemo } from 'react'
import * as R from 'ramda'
import routes from 'modules/routes'
import { useLocale } from 'locale'

function useRoutes () {
  const { __ } = useLocale()

  return useMemo(() => {
    function getFlatRoutes (routes, prefix = null) {
      if (routes && routes.length > 0) {
        return routes.reduce((s, x) => {
          const item = R.over(
            R.lensProp('path'),
            (p) =>
              R.mapObjIndexed((v, k) =>
                prefix
                  ? prefix[k] + v
                  : v
              , p),
            x
          )
          return R.reduce(R.concat, [], [s, [item], getFlatRoutes(x.routes, item.path)])
        }, [])
      }
      return []
    }

    const flatRoutes = getFlatRoutes(routes)

    function routeForSegment (segment) {
      let params = {}
      const result = R.findLast((x) =>
        R.findLast((p) => {
          const args = p.match(/:[a-zA-Z0-9]+/g)
          const re = args
            ? args.reduce((s, x) => s.replace(x, '([a-zA-Z0-9-]+)'), p)
            : p
          const search = segment.match ? segment.match(new RegExp('^' + re)) : null
          if (search !== null) {
            params = args
              ? R.fromPairs(R.zip(args.map((x) => x.substring(1)), search.slice(1)))
              : {}
            params.path = segment
            params.pathExtra = segment.replace(new RegExp('^' + re), '')
            return true
          }
          return false
        }, Object.values(x.path))
      , flatRoutes)
      if (result) {
        return R.assoc('params', params, result)
      }

      return null
    }

    function getLocalizedPath (path) {
      const route = path.en !== '/' ? routeForSegment(path) : '/'

      if (!route) {
        window.alert('404') // TODO: better 404 handler
        return
      }

      if (route !== '/') {
        const enParts = route.path.en.split('/')
        const destParts = __(route.path).split('/')
        const realPath = path.split('/').map((x, i) =>
          x === enParts[i]
            ? destParts[i]
            : x
        ).join('/')

        return realPath
      } else {
        return '/'
      }
    }

    return {
      routes,
      flatRoutes,
      routeForSegment,
      getLocalizedPath
    }
  }, [__])
}

export default useRoutes
