import { useContext } from 'react'
import { NavigationContext } from '../context'

function useNavigation () {
  return useContext(NavigationContext)
}

export default useNavigation
