import React from 'react'
import { useNavigation, useRoutes } from 'navigation/hooks'

function Link (props) {
  const { reset, to, children, ...otherProps } = props
  const { navigate } = useNavigation()
  const { getLocalizedPath } = useRoutes()

  return (
    <a href={to ? getLocalizedPath(to) : to} onClick={(e) => { e.preventDefault(); navigate(to, reset) }} {...otherProps}>
      {children}
    </a>
  )
}

export default Link
