import React, { useState, useEffect, useMemo, useLayoutEffect, useCallback } from 'react'
import * as R from 'ramda'
import { useLocale } from '../../locale'
import { useRoutes } from '../hooks'

const NavigationContext = React.createContext()

export function NavigationProvider (props) {
  const { children } = props

  const { routeForSegment, getLocalizedPath } = useRoutes()
  const { __ } = useLocale()

  const [history, setHistory] = useState([
    {
      location: document.location.pathname,
      index: 0,
      reset: false,
      title: ''
    }
  ])

  /*
   * handle back button
   */
  useEffect(() => {
    function handlePopState (e) {
      const index = e.state && e.state.index ? e.state.index : 0
      const reset = e.state && e.state.reset ? e.state.reset : false
      const lastIndex = history.length > 0 ? history[history.length - 1].index : 0
      const newHistory = lastIndex > index
        ? history.slice(0, -(lastIndex - index))
        : [
          ...history,
          {
            location: e.currentTarget.document.location.pathname,
            index,
            reset,
            title: ''
          }
        ]
      setHistory(newHistory)
    }
    window.addEventListener('popstate', handlePopState)

    return () => {
      window.removeEventListener('popstate', handlePopState)
    }
  }, [history])

  /*
   * find module and screens based on history
   */
  const { module, screens } = useMemo(() => {
    let screens = []
    let module = null
    history.forEach((historyEntry) => {
      const locationSegments = historyEntry.location
        .substring(1)
        .split('/')
        .map((x) => '/' + x)
        .filter((x, i) => i === 0 || x !== '/')
      const m = routeForSegment(locationSegments[0])
      const s = locationSegments.length > 1
        ? routeForSegment(historyEntry.location)
        : m.component ? m : null
      if (!module) {
        module = m
      }
      if (historyEntry.reset) {
        module = m
        screens = []
      }
      if (s) {
        screens.push({
          ...s,
          location: historyEntry.location,
          title: historyEntry.title
            ? R.mapObjIndexed((x) => x + ': ' + historyEntry.title.substring(0, 20), s.title)
            : s.title
        })
      }
    })

    console.trace('NavigationContext', '', { module: module.title.en, screens: screens.map((x) => x.title.en) })

    return { module, screens }
  }, [history, routeForSegment])

  /*
   * navigation functions
   */
  const navigate = useMemo(() => {
    return (path, reset = false) => {
      console.trace('NavigationContext', 'navigate', { path, reset })
      const localizedPath = getLocalizedPath(path)

      const index = history.length
      window.history.pushState({ index, reset }, '', localizedPath)
      setHistory([
        ...history,
        {
          location: localizedPath,
          index: index,
          reset,
          title: ''
        }
      ])
    }
  }, [getLocalizedPath, history])

  const pop = useCallback((n = 1) => {
    console.trace('NavigationContext', 'pop', { n })
    window.history.go(-n)
  }, [])

  const popTo = useCallback((path) => {
    console.trace('NavigationContext', 'popTo', { path })

    const localizedPath = getLocalizedPath(path)
    const historyIndex = R.findLastIndex((x) => x.location === localizedPath, history)

    if (historyIndex === -1) {
      // route is not in history, replace state
      window.history.replaceState({ index: history.length - 1, reset: false }, '', localizedPath)
      setHistory([
        ...history.slice(0, -1),
        {
          location: localizedPath,
          index: history.length - 1,
          reset: false,
          title: ''
        }
      ])
    } else {
      pop(history.length - historyIndex - 1)
    }
  }, [getLocalizedPath, history, pop])

  const setTitle = useCallback((title) => {
    console.trace('NavigationContext', 'setTitle', { title })
    if (history[history.length - 1].title !== title) {
      setHistory([
        ...history.slice(0, -1),
        {
          ...history[history.length - 1],
          title
        }
      ])
    }
  }, [history])

  

  const value = {
    module,
    screens,
    navigate,
    pop,
    popTo,
    setTitle,
    location: document.location.pathname
  }
  return (
    <NavigationContext.Provider value={value}>
      {children}
    </NavigationContext.Provider>
  )
}

export default NavigationContext
