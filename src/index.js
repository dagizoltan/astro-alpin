import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as serviceWorker from './serviceWorker'

require('theme/index.css')

global.React = React

ReactDOM.render(<App />, document.getElementById('root'))

serviceWorker.unregister()
